# bootstrap base rootfs

```
QmYYLGZopx6ZGeqpP3jxqRn9eLKDsE37Xotqc3ez1RCbfF centos-7.9.2009-rootfs.aarch64.tar.xz
QmSYt9d1WBNVjfgNNhyV4sNVqiM1yzVVzdXHWy63zhbGWf centos-7.6.1810-rootfs.x86_64.tar.xz
QmRPfci8NpK3onFvyEXn5np4cA7MRQX2Wsd3WwhSAGbcjT almalinux-8.8-rootfs.aarch64.tar.xz
QmaaQLJgbumjVEipHnpuG5J3ZRFRbTNV8G8v2Vfvniwx2S almalinux-8.8-rootfs.x86_64.tar.xz
QmQko9iZxNVUvADNWFhsp7eA5xDfZdWHNg6J5p7AjkMmXR voidlinux-rootfs.x86_64.tar.xz
QmPQUQBkR1WybgmjU22zTxhvhnapq2Wi6ZwAuoTLSkXnEe kylin-v10sp3-rootfs.x86_64.tar.xz
```
