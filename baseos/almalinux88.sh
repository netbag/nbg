wget -c https://mirrors.nju.edu.cn/almalinux-vault/8.8/BaseOS/$(arch)/kickstart/Packages/almalinux-release-8.8-1.el8.$(arch).rpm
mkdir -p rootfs/var/lib/rpm
rpm --root $PWD/rootfs --initdb
rpm --root $PWD/rootfs -ivh almalinux-release-*.rpm 
rm -f rootfs/etc/yum.repos.d/almalinux-*
echo nameserver 119.29.29.29 > rootfs/etc/resolv.conf
sed -i '/mirrorlist/d' rootfs/etc/yum.repos.d/almalinux.repo 
sed -i 's:# baseurl:baseurl:' rootfs/etc/yum.repos.d/almalinux.repo 
sed -i 's;https://repo.almalinux.org/almalinux;http://mirror.nju.edu.cn/almalinux-vault;' rootfs/etc/yum.repos.d/almalinux.repo 
yum --installroot $PWD/rootfs -y --nogpgcheck --exclude='*i686*' install bash yum  
mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc
chroot rootfs bash -c 'yum --releasever=8.8 --exclude="*i686*" -y install \
gcc patch tar zstd time tcsh environment-modules gzip git \
bzip2 xz gcc-gfortran gcc-c++ glibc-headers kernel-headers \
curl wget file python3 which perl autoconf automake flex bison rootfiles; \
yum clean all'
umount rootfs/proc
umount rootfs/dev
rm -rf rootfs/var/cache/yum/*
cat $0 >> rootfs/.nbg
tar cf almalinux-8.8-rootfs.$(arch).tar -C rootfs .
xz -9 -T 0 almalinux*.tar
