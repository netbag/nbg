```sh
apk add --no-progress bash make gawk grep sed diffutils patch tar libarchive-tools xz zstd bzip2 gzip which
```

```sh
# x86_64
curl https://get.hpc.dev/r/x.tar.xz | tar Jxf -
# aarch64 
curl https://get.hpc.dev/r/a.tar.xz | tar Jxf -
# https://gateway.pinata.cloud/ipfs/QmX6h81JPeuY8Xix9oR8mVaxvyrQJcL6qPV5ThJWYTTamK/
chroot . sh -c 'echo nameserver 9.9.9.9 > /etc/resolv.conf; apk add --no-progress bash make gawk sed cmake make patch git curl wget perl'
# dev
curl https://get.hpc.dev/r/x.tar.xz | tar Jxf -
chroot . sh -c 'echo nameserver 9.9.9.9 > /etc/resolv.conf; apk add --no-progress bash make gawk sed cmake make patch git curl wget perl python3 pkgconfig flex bison procps'

```
