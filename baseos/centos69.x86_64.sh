ROOTFS=rootfs
mkdir -p $ROOTFS/var/lib/rpm
rpm --root $PWD/$ROOTFS --initdb
rpm --root $PWD/$ROOTFS -ivh https://vault.centos.org/6.9/os/x86_64/Packages/centos-release-6-9.el6.12.3.x86_64.rpm

rm -f $ROOTFS/etc/yum.repos.d/*.repo

cat <<EOF > $ROOTFS/etc/yum.repos.d/vault.repo
[vault]
name=CentOS - Vault
baseurl=https://vault.centos.org/6.9/os/x86_64/
gpgcheck=0
EOF

yum --disablerepo="*" --enablerepo=vault --installroot $PWD/$ROOTFS -y --nogpgcheck install yum

echo 'nameserver 119.29.29.29' > $ROOTFS/etc/resolv.conf

mount -o bind /dev $ROOTFS/dev
mount -t proc none $ROOTFS/proc
cat << EOF | chroot $ROOTFS
export PATH=/sbin:/bin:/usr/sbin:/usr/bin
rm -rf /var/lib/rpm/*
rpm --initdb
yum --disablerepo=* --enablerepo=vault update
yum --disablerepo=* --enablerepo=vault -y install yum rootfiles
yum clean all
EOF

umount $ROOTFS/proc
umount $ROOTFS/dev
