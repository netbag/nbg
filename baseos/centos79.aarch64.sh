wget -c https://mirrors.nju.edu.cn/centos-vault/altarch/7.9.2009/os/aarch64/Packages/centos-release-7-9.2009.0.el7.centos.aarch64.rpm
mkdir -p rootfs/var/lib/rpm
rpm --root $PWD/rootfs -ivh *.rpm --force --nodeps

rm -f rootfs/etc/yum.repos.d/*.repo
cat <<'EOF' > rootfs/etc/yum.repos.d/el79.repo
[el79]
name=el79
baseurl=https://mirrors.nju.edu.cn/centos-vault/altarch/7.9.2009/os/aarch64/
gpgcheck=0
enabled=1
EOF

yum --installroot $PWD/rootfs -y --nogpgcheck install yum rootfiles
echo nameserver 114.114.114.114 > rootfs/etc/resolv.conf
mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc
chroot rootfs bash -c 'yum -y install yum rootfiles wget tar gzip bzip2 xz patch file unzip which vim-minimal gcc kernel-headers gcc-c++ gcc-gfortran flex bison environment-modules python3'
umount rootfs/dev
umount rootfs/proc
rm -rf rootfs/var/cache/yum/*
tar cf centos-7.9.2009-rootfs.aarch64.tar -C rootfs .
xz -9 -T 0 centos-7.9.2009-rootfs.aarch64.tar
