#! /bin/bash
mkdir -p rootfs/var/lib/rpm
rpm --root $PWD/rootfs --initdb
rpm --root $PWD/rootfs -ivh https://mirrors.nju.edu.cn/centos-vault/7.9.2009/os/x86_64/Packages/centos-release-7-9.2009.0.el7.centos.x86_64.rpm

cat <<'EOF' > rootfs/etc/yum.repos.d/vault.repo
[vault]
name= vault
baseurl=https://mirrors.nju.edu.cn/centos-vault/7.9.2009/os/x86_64/
gpgcheck=0
EOF

yum --installroot $PWD/rootfs --nogpgcheck --disablerepo=* --enablerepo=vault install -y yum

echo 'nameserver 119.29.29.29' > rootfs/etc/resolv.conf
mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc
cat << EOF | chroot rootfs
export PATH=/sbin:/bin:/usr/sbin:/usr/bin
rm -rf /var/lib/rpm/*
rpm --initdb
yum --disablerepo=* --enablerepo=vault install -y yum rootfiles
yum clean all
EOF
umount rootfs/proc
umount rootfs/dev
