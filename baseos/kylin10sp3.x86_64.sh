#! /bin/bash
# Qmf9C52keXQZiBcJvHmhC82Ry4auVgE5KESTDtXY66HQDj Kylin-Server-V10-SP3-General-Release-2303-X86_64.iso
ISO=Kylin-Server-V10-SP3-General-Release-2303-X86_64.iso
mkdir -p rootdir/media/cdrom
mount $ISO rootdir/media/cdrom
ln -sf $PWD/rootdir/media/cdrom /media

mkdir -p rootdir/var/lib/rpm
rpm --root $PWD/rootdir --initdb
rpm --root $PWD/rootdir --install --force --nodeps --noscripts \
  /media/cdrom/Packages/kylin-release-*.x86_64.rpm \
  /media/cdrom/Packages/kylin-repos-*.x86_64.rpm \
  /media/cdrom/Packages/kylin-gpg-keys-*.x86_64.rpm 

cat <<'EOF' > rootdir/etc/yum.repos.d/media.repo
[media]
name= Media
baseurl=file:///media/cdrom
gpgcheck=0
EOF

yum --installroot $PWD/rootdir --nogpgcheck --disablerepo=* --enablerepo=media install -y yum 
mount -o bind /dev rootdir/dev
mount -t proc none rootdir/proc
cat << EOF | chroot rootdir
export PATH=/sbin:/bin:/usr/sbin:/usr/bin
rm -rf /var/lib/rpm/*
rpm --initdb
yum clean all
yum --disablerepo=* --enablerepo=media update
yum --disablerepo=* --enablerepo=media -y install yum rootfiles
yum clean all
EOF

umount rootdir/proc
umount rootdir/dev
umount rootdir/media/cdrom
rm /media/cdrom
