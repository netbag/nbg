#! /bin/bash
ISO=openEuler-22.03-LTS-SP4-x86_64-dvd.iso
mkdir -p rootdir/media/cdrom
mount $ISO rootdir/media/cdrom
ln -sf $PWD/rootdir/media/cdrom /media

mkdir -p rootdir/var/lib/rpm
rpm --root $PWD/rootdir --initdb
rpm --root $PWD/rootdir --install --force --nodeps --noscripts \
  /media/cdrom/Packages/openEuler-release-*.x86_64.rpm \
  /media/cdrom/Packages/openEuler-repos-*.x86_64.rpm \
  /media/cdrom/Packages/openEuler-gpg-keys-*.x86_64.rpm 

cat <<'EOF' > rootdir/etc/yum.repos.d/media.repo
[media]
name= Media
baseurl=file:///media/cdrom
gpgcheck=0
EOF

yum --installroot $PWD/rootdir --nogpgcheck --disablerepo=* --enablerepo=media install -y yum 
mount -o bind /dev rootdir/dev
mount -t proc none rootdir/proc
cat << EOF | chroot rootdir
export PATH=/sbin:/bin:/usr/sbin:/usr/bin
rm -rf /var/lib/rpm/*
rpm --initdb
yum clean all
yum --disablerepo=* --enablerepo=media update
yum --disablerepo=* --enablerepo=media -y install yum rootfiles
yum clean all
EOF

umount rootdir/proc
umount rootdir/dev
umount rootdir/media/cdrom

rm /media/cdrom
