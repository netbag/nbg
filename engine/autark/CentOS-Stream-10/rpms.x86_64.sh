mount CentOS-Stream-10-20240822.0-x86_64-dvd1.iso m
find m |grep 14.2.1-1.el10 | while read f; do cp $f r; done
find m |grep 2.39-17.el10 |grep -v langpack | while read f; do cp $f r; done
find m |grep kernel-headers | while read f; do cp $f r; done
find m |grep gmp | grep 6.2.1 | while read f; do cp $f r; done
find m |grep libmpc | while read f; do cp $f r; done
find m |grep mpfr |  while read f; do cp $f r; done
cp m/BaseOS/Packages/zlib-ng-compat-*.x86_64.rpm r
cp m/BaseOS/Packages/libzstd-*.x86_64.rpm r
cp m/BaseOS/Packages/binutils-*.x86_64.rpm r
cp m/BaseOS/Packages/jansson-*.x86_64.rpm r
rm -f r/*-docs-* r/*-docs-* r/*offload* r/*source* r/gcc-plugin*
rm -f r/glibc-doc-* r/glibc-gconv-extra* r/gmp-c++-* r/gmp-devel-* 
rm -f r/libgccjit-* r/libitm-devel* r/libmpc-devel* r/mpfr-devel-*
rm -f r/binutils-gold-* r/glibc-utils-*
