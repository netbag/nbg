# mixroot : hybrid sysroot, coexistence of multiple versions of glibc/sysroot 

```
# compile cmake, test CC and CXX
export CTEST_PARALLEL_LEVEL=16
./bootstrap -- -DCMAKE_USE_OPENSSL=OFF
make test
```
```
# compile hdf5, test CC, FC and CXX
./configure --enable-cxx --enable-fortran
```
```
QmeyUYewHFkPGnrtLCa1P5JsQaR2cbvP5CbZ3jrssuK8cw cmake-3.20.0.tar.gz
QmejR7S7zQofx3FaQa59iCmzguSzVPwYERprarCzDhh7gK hdf5-1.14.4-3.tar.gz
QmdjtJe4MkH2ts5FH6YDms7C6QdyXtDytvU68wp25kGL2v lapack-3.12.0.tar.gz
```
patch intel compiler
```sh
#! /bin/bash
SYSROOT=$1
find . -type f -executable | while read F ; do 
  if head -c 4 $F | grep -q ELF && patchelf --print-interpreter $F >&/dev/null; then
    patchelf --set-interpreter $SYSROOT/lib64/ld-linux-x86-64.so.2 $F 
    patchelf --set-rpath $SYSROOT/lib/x86_64-linux-gnu:$SYSROOT/usr/lib/x86_64-linux-gnu $F
  fi
done

find . -type f -name 'lib*so*' | while read F; do
  if head -c 4 $F | grep -q ELF && patchelf --print-needed $F | grep -q lib; then
    patchelf --set-rpath $SYSROOT/lib/x86_64-linux-gnu:$SYSROOT/usr/lib/x86_64-linux-gnu $F
  fi
done

find . -type f -name '*.so' | while read F; do
  if head -c 4 $F | grep -q ELF && patchelf --print-needed $F | grep -q lib; then
    patchelf --set-rpath $SYSROOT/lib/x86_64-linux-gnu:$SYSROOT/usr/lib/x86_64-linux-gnu $F
  fi
done
```
```sh
echo "-isystem$SYSROOT/usr/include/x86_64-linux-gnu" > icc.cfg
echo "-isystem$SYSROOT/usr/include/x86_64-linux-gnu" > ifort.cfg
echo "-isystem$SYSROOT/usr/include/x86_64-linux-gnu -isystem$SYSROOT/usr/include/x86_64-linux-gnu/c++/10/" > icpc.cfg
```
fix LLVM absolute linker path, -L/lib, -L/usr/lib
```sh
#! /bin/sh
if echo "$@" | grep -q -- 'dynamic-linker /lib64/ld-linux-x86-64.so.2'; then
  exec $SYSROOT/usr/bin/x86_64-linux-gnu-ld.bfd --sysroot=$SYSROOT -dynamic-linker $SYSROOT/lib64/ld-linux-x86-64.so.2 -rpath '$ORIGIN/../lib:$SYSROOT/lib/x86_64-linux-gnu:$SYSROOT/usr/lib/x86_64-linux-gnu' -L$SYSROOT/lib/x86_64-linux-gnu -L$SYSROOT/usr/lib/x86_64-linux-gnu $(echo "$@"| sed 's:-dynamic-linker /lib64/ld-linux-x86-64.so.2::;s:-L/usr/lib ::;s:-L/lib ::;')
else
  exec $SYSROOT/usr/bin/x86_64-linux-gnu-ld.bfd -rpath '$ORIGIN/../lib:$SYSROOT/lib/x86_64-linux-gnu:$SYSROOT/usr/lib/x86_64-linux-gnu' "$@"
fi
```
```
==> oneapi.2402/compiler/2024.2/bin/icpx.cfg <==
-B $SYSROOT/lib/x86_64-linux-gnu 
-B $SYSROOT/usr/lib/x86_64-linux-gnu 
--sysroot=$SYSROOT

==> oneapi.2402/compiler/2024.2/bin/icx.cfg <==
-B $SYSROOT/lib/x86_64-linux-gnu 
-B $SYSROOT/usr/lib/x86_64-linux-gnu 
--sysroot=$SYSROOT

==> oneapi.2402/compiler/2024.2/bin/ifort.cfg <==
-diag-disable=10448
-B $SYSROOT/lib/x86_64-linux-gnu 
-B $SYSROOT/usr/lib/x86_64-linux-gnu 
--sysroot=$SYSROOT

==> oneapi.2402/compiler/2024.2/bin/ifx.cfg <==
-B $SYSROOT/lib/x86_64-linux-gnu 
-B $SYSROOT/usr/lib/x86_64-linux-gnu 
--sysroot=$SYSROOT

```
AOCC compiler
```sh
cd bin; mkdir real real2
mv ld.lld real2
mv lld real2

ln -sf ld.lld ld
ln -sf ld.lld lld

ln -sf ../clang-14 real/clang
ln -sf ../clang-14 real/flang
ln -sf ../clang-14 real/clang++

rm clang clang++ flang

cat<<'EOF' > ld.lld
#! /bin/sh
exec $AOCC_ROOT/bin/real2/ld.lld --sysroot=$SYSROOT -dynamic-linker $SYSROOT/lib64/ld-linux-x86-64.so.2 -rpath '$ORIGIN/../lib:$SYSROOT/lib/x86_64-linux-gnu:$SYSROOT/usr/lib/x86_64-linux-gnu' -L$AOCC_ROOT/lib -L$SYSROOT/lib/x86_64-linux-gnu -L$SYSROOT/usr/lib/x86_64-linux-gnu $(echo "$@"| sed 's:-dynamic-linker /lib64/ld-linux-x86-64.so.2::;s:-L/usr/lib ::;s:-L/lib ::;')
EOF
chmod +x ld.lld

cat <<'EOF' > clang
#! /bin/sh
exec $AOCC_ROOT/bin/real/clang --sysroot=$SYSROOT "$@"
EOF
chmod +x clang

cat <<'EOF' > clang++
#! /bin/sh
exec $AOCC_ROOT/bin/real/clang++ --sysroot=$SYSROOT "$@"
EOF
chmod +x clang++

cat <<'EOF' > flang
#! /bin/sh
exec $AOCC_ROOT/bin/real/flang --sysroot=$SYSROOT -isystem $AOCC_ROOT/include "$@"
EOF
chmod +x flang
```
