wget https://mirror.nju.edu.cn/almalinux-vault/8.5/BaseOS/aarch64/kickstart/Packages/almalinux-release-8.5-2.el8.aarch64.rpm
mkdir -p rootfs/var/lib/rpm
rpm --root $PWD/rootfs --initdb
rpm --root $PWD/rootfs -ivh almalinux-release-8.5-2.el8.aarch64.rpm 
rm -f rootfs/etc/yum.repos.d/almalinux-*
sed -i '/mirrorlist/d' rootfs/etc/yum.repos.d/almalinux.repo 
sed -i 's:# baseurl:baseurl:' rootfs/etc/yum.repos.d/almalinux.repo 
sed -i 's;https://repo.almalinux.org/almalinux;http://mirror.nju.edu.cn/almalinux-vault;' rootfs/etc/yum.repos.d/almalinux.repo 
yum --installroot $PWD/rootfs -y --nogpgcheck install rootfiles yum yum-utils

# no libquadmath on aarch64
yumdownloader --releasever=8.5 zlib.aarch64 binutils.aarch64 libmpc.aarch64 gmp.aarch64 glibc.aarch64 gcc-gfortran.aarch64 gcc-c++.aarch64 libstdc++.aarch64 glibc-common.aarch64 libgcc.aarch64 libxcrypt.aarch64 libasan.aarch64 libatomic.aarch64 libgomp.aarch64 libubsan.aarch64 mpfr.aarch64 mpfr-devel.aarch64 libgfortran.aarch64 glibc-devel.aarch64 kernel-headers.aarch64 libstdc++-devel.aarch64 gcc.aarch64 libxcrypt-devel.aarch64 cpp.aarch64 glibc-headers.aarch64

rm -rf ./usr/lib/.build-id/
ln -sf ../../../../../lib64/libgcc_s.so.1 usr/lib/gcc/aarch64-redhat-linux/8/libgcc_s.so
