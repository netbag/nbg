wget https://mirror.nju.edu.cn/almalinux-vault/8.5/BaseOS/x86_64/kickstart/Packages/almalinux-release-8.5-2.el8.x86_64.rpm
mkdir -p rootfs/var/lib/rpm
rpm --root $PWD/rootfs --initdb
rpm --root $PWD/rootfs -ivh almalinux-release-8.5-2.el8.x86_64.rpm 
rm -f rootfs/etc/yum.repos.d/almalinux-*
sed -i '/mirrorlist/d' rootfs/etc/yum.repos.d/almalinux.repo 
sed -i 's:# baseurl:baseurl:' rootfs/etc/yum.repos.d/almalinux.repo 
sed -i 's;https://repo.almalinux.org/almalinux;http://mirror.nju.edu.cn/almalinux-vault;' rootfs/etc/yum.repos.d/almalinux.repo 
yum --installroot $PWD/rootfs -y --nogpgcheck install rootfiles yum yum-utils


yumdownloader --releasever=8.5 zlib.x86_64 binutils.x86_64 libmpc.x86_64 gmp.x86_64 glibc.x86_64 gcc-gfortran.x86_64 gcc-c++.x86_64 libstdc++.x86_64 glibc-common.x86_64 libgcc.x86_64 libxcrypt.x86_64 libasan.x86_64 libatomic.x86_64 libgomp.x86_64 libubsan.x86_64 mpfr.x86_64 mpfr-devel.x86_64 libgfortran.x86_64 glibc-devel.x86_64 kernel-headers.x86_64 libstdc++-devel.x86_64 gcc.x86_64 libxcrypt-devel.x86_64 libquadmath.x86_64 libquadmath-devel.x86_64 cpp.x86_64 glibc-headers.x86_64

rm -rf ./usr/lib/gcc/x86_64-redhat-linux/8/32/
rm -rf ./usr/lib/.build-id
