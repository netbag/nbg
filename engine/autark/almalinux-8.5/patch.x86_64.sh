#! /bin/bash
SYSROOT=$PWD
#patchelf 0.14.5

find . -type f -executable | while read F ; do 
  if head -c 4 $F | grep -q ELF && patchelf --print-interpreter $F >&/dev/null; then
    patchelf --set-interpreter $SYSROOT/lib64/ld-linux-x86-64.so.2 $F 
    patchelf --set-rpath $SYSROOT/lib64:$SYSROOT/usr/lib64 $F
  fi
done

find . -type f -name 'lib*so*' | while read F; do
  if head -c 4 $F | grep -q ELF && patchelf --print-needed $F | grep -q lib; then
    patchelf --set-rpath $SYSROOT/lib64:$SYSROOT/usr/lib64 $F
  fi
done

$SYSROOT/usr/bin/gcc -dumpspecs | \
sed "/ld-linux-x86-64.so.2/s:/lib64/ld-linux-x86-64.so.2:$SYSROOT&:g" \
> $(dirname $($SYSROOT/usr/bin/gcc -print-libgcc-file-name))/specs
 
rm -f usr/bin/ld
echo -e "#! /bin/sh\nexec $SYSROOT/usr/bin/ld.bfd -rpath '\$ORIGIN/../lib:$SYSROOT/lib64:$SYSROOT/usr/lib64' \"\$@\""\
> usr/bin/ld
chmod +x usr/bin/ld

for F in gcc g++ c++ gfortran; do 
  mv usr/bin/$F usr/bin/$F.bin
  echo -e "#! /bin/sh\nexec $SYSROOT/usr/bin/$F.bin --sysroot=$SYSROOT \"\$@\"" > usr/bin/$F
  chmod +x usr/bin/$F
done

sed -i "1c#! $(which bash)" usr/bin/ldd
sed -i "/RTLDLIST=/cRTLDLIST=\"$SYSROOT/lib64/ld-linux-x86-64.so.2\"" usr/bin/ldd
