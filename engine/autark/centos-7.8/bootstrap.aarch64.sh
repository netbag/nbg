wget https://mirror.nju.edu.cn/centos-vault/altarch/7.8.2003/os/aarch64/Packages/centos-release-7-8.2003.0.el7.centos.aarch64.rpm

mkdir -p rootfs/var/lib/rpm
rpm --root $PWD/rootfs -ivh *.rpm --nodeps

rm -f rootfs/etc/yum.repos.d/*.repo
cat <<'EOF' > rootfs/etc/yum.repos.d/base.repo
[base]
name=Base
baseurl=http://mirror.nju.edu.cn/centos-vault/altarch/7.8.2003/os/aarch64
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
enabled=1
EOF

yumdownloader --installroot $PWD/rootfs -y --nogpgcheck zlib.aarch64 binutils.aarch64 libmpc.aarch64 gmp.aarch64 glibc.aarch64 gcc-gfortran.aarch64 gcc-c++.aarch64 libstdc++.aarch64 glibc-common.aarch64 libgcc.aarch64 libatomic.aarch64 libgomp.aarch64  mpfr.aarch64 mpfr-devel.aarch64 libgfortran.aarch64 glibc-devel.aarch64 kernel-headers.aarch64 libstdc++-devel.aarch64 gcc.aarch64 cpp.aarch64 glibc-headers.aarch64

find . -xtype l
ln -sf ../../../../../lib64/libgcc_s.so.1  ./usr/lib/gcc/aarch64-redhat-linux/4.8.2/libgcc_s.so
