wget https://mirror.nju.edu.cn/centos-vault/7.8.2003/os/x86_64/Packages/centos-release-7-8.2003.0.el7.centos.x86_64.rpm
mkdir -p rootfs/var/lib/rpm
rpm --root $PWD/rootfs -ivh *.rpm

rm -f rootfs/etc/yum.repos.d/*.repo
cat <<'EOF' > rootfs/etc/yum.repos.d/base.repo
[base]
name=Base
baseurl=http://mirror.nju.edu.cn/centos-vault/7.8.2003/os/x86_64/
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
enabled=1
EOF

yumdownloader --installroot $PWD/rootfs -y --nogpgcheck zlib.x86_64 binutils.x86_64 libmpc.x86_64 gmp.x86_64 glibc.x86_64 gcc-gfortran.x86_64 gcc-c++.x86_64 libstdc++.x86_64 glibc-common.x86_64 libgcc.x86_64 libasan.x86_64 libatomic.x86_64 libgomp.x86_64  mpfr.x86_64 mpfr-devel.x86_64 libgfortran.x86_64 glibc-devel.x86_64 kernel-headers.x86_64 libstdc++-devel.x86_64 gcc.x86_64 libquadmath.x86_64 libquadmath-devel.x86_64 cpp.x86_64 glibc-headers.x86_64

find . -xtype l
rm -rf ./usr/lib/gcc/x86_64-redhat-linux/4.8.2/32/
ln -sf ../../../../../lib64/libgcc_s.so.1 ./usr/lib/gcc/x86_64-redhat-linux/4.8.2/libgcc_s.so
