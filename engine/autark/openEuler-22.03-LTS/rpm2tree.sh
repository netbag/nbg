# install yumdownloader
yum install dnf-plugins-core

yumdownloader  --resolve \
zlib.aarch64 binutils.aarch64 libmpc.aarch64 gmp.aarch64 \
glibc.aarch64 gcc-gfortran.aarch64 gcc-c++.aarch64 \
libstdc++.aarch64 glibc-common.aarch64 libgcc.aarch64 \
libxcrypt.aarch64 libasan.aarch64 libatomic.aarch64 \
libgomp.aarch64 libubsan.aarch64 mpfr.aarch64 mpfr-devel.aarch64

yumdownloader  --resolve \
zlib.x86_64 binutils.x86_64 libmpc.x86_64 gmp.x86_64 \
glibc.x86_64 gcc-gfortran.x86_64 gcc-c++.x86_64 \
libstdc++.x86_64 glibc-common.x86_64 libgcc.x86_64 \
libxcrypt.x86_64 libasan.x86_64 libatomic.x86_64 \
libgomp.x86_64 libubsan.x86_64 mpfr.x86_64 mpfr-devel.x86_64

ls ../rpms/*.rpm -1|while read f; do bsdtar xf $f; done

find . -xtype l
cd usr/lib64
find . -xtype l|xargs -n 1 basename|while read f; do ln -sf gconv/$f .; done
