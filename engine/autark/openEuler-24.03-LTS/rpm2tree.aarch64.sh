yum -y install dnf-plugins-core bsdtar

yumdownloader  --resolve zlib.aarch64 binutils.aarch64 libmpc.aarch64 \
gmp.aarch64 glibc.aarch64 gcc-gfortran.aarch64 gcc-c++.aarch64 \
libstdc++.aarch64 glibc-common.aarch64 libgcc.aarch64 \
libxcrypt.aarch64 libasan.aarch64 libatomic.aarch64 \
libgomp.aarch64 libubsan.aarch64 mpfr.aarch64 mpfr-devel.aarch64

# ld.bfd require libjansson.so.4
yumdownloader  --resolve jansson.aarch64

repoquery --whatrequires libtool-ltdl
rm -f gc-* guile-* libtool-ltdl-* make-*

ls ../sysrpms/*.rpm -1|while read f; do bsdtar xf $f; done

# fix broken links 
cd usr/lib64

find . -type l | sed 's:./::' | while read l; do
  readlink $l|grep -q '^/usr/' && ln -sf gconv/$l .
done
