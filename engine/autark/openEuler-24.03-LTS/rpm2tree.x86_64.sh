yum -y install dnf-plugins-core bsdtar

yumdownloader  --resolve zlib.x86_64 binutils.x86_64 libmpc.x86_64 \
gmp.x86_64 glibc.x86_64 gcc-gfortran.x86_64 gcc-c++.x86_64 \
libstdc++.x86_64 glibc-common.x86_64 libgcc.x86_64 \
libxcrypt.x86_64 libasan.x86_64 libatomic.x86_64 \
libgomp.x86_64 libubsan.x86_64 mpfr.x86_64 mpfr-devel.x86_64

# ld.bfd require libjansson.so.4
yumdownloader  --resolve jansson.x86_64

repoquery --whatrequires libtool-ltdl
rm -f gc-* guile-* libtool-ltdl-* make-*

ls ../sysrpms/*.rpm -1|while read f; do bsdtar xf $f; done

# fix broken links 
cd usr/lib64
find . -xtype l|xargs -n 1 basename|while read f; do ln -sf gconv/$f .; done
