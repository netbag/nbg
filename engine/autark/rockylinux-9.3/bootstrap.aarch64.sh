wget https://mirror.nju.edu.cn/rocky-vault/9.3/BaseOS/aarch64/os/Packages/r/rocky-gpg-keys-9.3-1.3.el9.noarch.rpm
wget https://mirror.nju.edu.cn/rocky-vault/9.3/BaseOS/aarch64/os/Packages/r/rocky-release-9.3-1.3.el9.noarch.rpm
wget https://mirror.nju.edu.cn/rocky-vault/9.3/BaseOS/aarch64/os/Packages/r/rocky-repos-9.3-1.3.el9.noarch.rpm

mkdir -p rootfs/var/lib/rpm
rpm --root $PWD/rootfs -ivh rocky*.rpm
sed -i '/mirrorlist/d' rootfs/etc/yum.repos.d/*
sed -i 's;http://dl.rockylinux.org/$contentdir/$releasever/;http://mirror.nju.edu.cn/rocky-vault/9.3/;' rootfs/etc/yum.repos.d/*
sed -i 's:#baseurl:baseurl:' rootfs/etc/yum.repos.d/*

yumdownloader --installroot $PWD/rootfs -y --nogpgcheck \
zlib.aarch64 binutils.aarch64 libmpc.aarch64 gmp.aarch64 glibc.aarch64 \
gcc-gfortran.aarch64 gcc-c++.aarch64 libstdc++.aarch64 glibc-common.aarch64 \
libgcc.aarch64 libxcrypt.aarch64 libasan.aarch64 libatomic.aarch64 \
libgomp.aarch64 libubsan.aarch64 mpfr.aarch64 mpfr-devel.aarch64 \
libgfortran.aarch64 glibc-devel.aarch64 kernel-headers.aarch64 \
libstdc++-devel.aarch64 gcc.aarch64 libxcrypt-devel.aarch64 \
cpp.aarch64 glibc-headers.aarch64

ls ../*.rpm| xargs -n 1 bsdtar xf
rm -rf ./usr/lib/.build-id/
