wget https://mirror.nju.edu.cn/rocky-vault/9.3/BaseOS/x86_64/os/Packages/r/rocky-gpg-keys-9.3-1.3.el9.noarch.rpm
wget https://mirror.nju.edu.cn/rocky-vault/9.3/BaseOS/x86_64/os/Packages/r/rocky-release-9.3-1.3.el9.noarch.rpm
wget https://mirror.nju.edu.cn/rocky-vault/9.3/BaseOS/x86_64/os/Packages/r/rocky-repos-9.3-1.3.el9.noarch.rpm

mkdir -p rootfs/var/lib/rpm
rpm --root $PWD/rootfs -ivh rocky*.rpm
sed -i '/mirrorlist/d' rootfs/etc/yum.repos.d/*
sed -i 's;http://dl.rockylinux.org/$contentdir/$releasever/;http://mirror.nju.edu.cn/rocky-vault/9.3/;' rootfs/etc/yum.repos.d/*
sed -i 's:#baseurl:baseurl:' rootfs/etc/yum.repos.d/*

yumdownloader --installroot $PWD/rootfs -y --nogpgcheck \
zlib.x86_64 binutils.x86_64 libmpc.x86_64 gmp.x86_64 glibc.x86_64 \
gcc-gfortran.x86_64 gcc-c++.x86_64 libstdc++.x86_64 glibc-common.x86_64 \
libgcc.x86_64 libxcrypt.x86_64 libasan.x86_64 libatomic.x86_64 \
libgomp.x86_64 libubsan.x86_64 mpfr.x86_64 mpfr-devel.x86_64 \
libgfortran.x86_64 glibc-devel.x86_64 kernel-headers.x86_64 \
libstdc++-devel.x86_64 gcc.x86_64 libxcrypt-devel.x86_64 \
libquadmath.x86_64 libquadmath-devel.x86_64 cpp.x86_64 glibc-headers.x86_64


ls ../*.rpm| xargs -n 1 bsdtar xf
rm -rf ./usr/lib/.build-id/
rm -rf ./usr/lib/gcc/x86_64-redhat-linux/11/32/
