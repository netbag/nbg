apt install --reinstall --download-only libmpfr4 \
libmpc3 libc-dev-bin binutils libc-bin linux-libc-dev \
libgmp10 gfortran-5 gcc-5 g++-5 libgomp1 zlib1g libisl15 \
libgcc-5-dev libzstd1 libstdc++6 libc6 libstdc++6 libgcc1

rm manpages*

ls ../sysdebs/*.deb|while read f; do dpkg -x $f .; done
rm -rf usr/share/bug usr/share/man usr/share/doc

# x86_64
ln -sf ..$(readlink lib64/ld-linux-x86-64.so.2) lib64/ld-linux-x86-64.so.2

find usr/lib/x86_64-linux-gnu/ -type l | while read F; do
    readlink $F |grep -q '/lib/' && ln -sf ../../..$(readlink $F) $F
done
ln -sf ../../../../../lib/x86_64-linux-gnu/libgcc_s.so.1 ./usr/lib/gcc/x86_64-linux-gnu/5/libgcc_s.so

# aarch64

find usr/lib/aarch64-linux-gnu/ -type l | while read F; do 
   readlink $F |grep -q '/lib/' && ln -sf ../../..$(readlink $F) $F
done

ln -sf ../../../../../lib/aarch64-linux-gnu/libgcc_s.so.1 ./usr/lib/gcc/aarch64-linux-gnu/5/libgcc_s.so

find . -name 'lib*.so*'|xargs chmod +x
