#! /bin/bash
# patchelf 0.15.0
SYSROOT=$PWD
find . -type f -executable | while read F ; do 
  if head -c 4 $F | grep -q ELF && patchelf --print-interpreter $F >&/dev/null; then
    patchelf --set-interpreter $SYSROOT/lib/ld-linux-aarch64.so.1 $F 
    patchelf --set-rpath $SYSROOT/lib/aarch64-linux-gnu:$SYSROOT/usr/lib/aarch64-linux-gnu $F
  fi
done

find . -type f -name 'lib*so*' | while read F; do
  if head -c 4 $F | grep -q ELF && patchelf --print-needed $F | grep -q lib; then
    patchelf --set-rpath $SYSROOT/lib/aarch64-linux-gnu:$SYSROOT/usr/lib/aarch64-linux-gnu $F
  fi
done

$SYSROOT/usr/bin/gcc-5 -dumpspecs | \
sed "/ld-linux-aarch64/s:/lib/ld-linux-aarch64:$SYSROOT&:g" \
> $(dirname $($SYSROOT/usr/bin/gcc-5 -print-libgcc-file-name))/specs
 
rm -f usr/bin/ld
echo -e "#! /bin/sh\nexec $SYSROOT/usr/bin/ld.bfd -rpath '\$ORIGIN/../lib:$SYSROOT/lib/aarch64-linux-gnu:$SYSROOT/usr/lib/aarch64-linux-gnu' \"\$@\""\
> usr/bin/ld
chmod +x usr/bin/ld

for F in cpp gcc g++ gfortran; do 
  echo -e "#! /bin/sh\nexec $SYSROOT/usr/bin/$F-5 --sysroot=$SYSROOT \"\$@\"" > usr/bin/$F
  chmod +x usr/bin/$F
done

sed -i "1c#! $(which bash)" usr/bin/ldd
sed -i "/RTLDLIST=/cRTLDLIST=\"$SYSROOT/lib/ld-linux-aarch64.so.1\"" usr/bin/ldd
