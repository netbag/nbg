#! /bin/bash
# patchelf 0.14.5
SYSROOT=$PWD

find . -type f -executable | while read F ; do 
  if head -c 4 $F | grep -q ELF && patchelf --print-interpreter $F >&/dev/null; then
    patchelf --set-interpreter $SYSROOT/lib64/ld-linux-x86-64.so.2 $F 
    patchelf --set-rpath $SYSROOT/lib/x86_64-linux-gnu:$SYSROOT/usr/lib/x86_64-linux-gnu $F
  fi
done

find . -type f -name 'lib*so*' | while read F; do
  if head -c 4 $F | grep -q ELF && patchelf --print-needed $F | grep -q lib; then
    patchelf --set-rpath $SYSROOT/lib/x86_64-linux-gnu:$SYSROOT/usr/lib/x86_64-linux-gnu $F
  fi
done

$SYSROOT/usr/bin/gcc-5 -dumpspecs | \
sed "/ld-linux-x86-64.so.2/s:/lib64/ld-linux-x86-64.so.2:$SYSROOT&:g" \
> $(dirname $($SYSROOT/usr/bin/gcc-5 -print-libgcc-file-name))/specs
 
rm -f usr/bin/ld
echo -e "#! /bin/sh\nexec $SYSROOT/usr/bin/ld.bfd -rpath '\$ORIGIN/../lib:$SYSROOT/lib/x86_64-linux-gnu:$SYSROOT/usr/lib/x86_64-linux-gnu' \"\$@\""\
> usr/bin/ld
chmod +x usr/bin/ld

for F in cpp gcc g++ gfortran; do 
  echo -e "#! /bin/sh\nexec $SYSROOT/usr/bin/$F-5 --sysroot=$SYSROOT \"\$@\"" > usr/bin/$F
  chmod +x usr/bin/$F
done

sed -i "1c#! $(which bash)" usr/bin/ldd
sed -i "/RTLDLIST=/cRTLDLIST=\"$SYSROOT/lib64/ld-linux-x86-64.so.2\"" usr/bin/ldd
