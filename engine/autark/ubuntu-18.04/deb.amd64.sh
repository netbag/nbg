apt install --reinstall --download-only libmpfr6 libmpc3 \
libc-dev-bin binutils libc-bin linux-libc-dev gfortran-7 \
gcc-7 g++-7 libgomp1 zlib1g libisl19 libgmp10 libc6 libstdc++6 libgcc1

cp /var/cache/apt/archives/*.deb .
rm -f manpage*

find ../debs -type f|xargs -n 1 -i dpkg -x {} .
rm -rf usr/share/bug usr/share/man usr/share/doc

ln -sf ..$(readlink lib64/ld-linux-x86-64.so.2) lib64/ld-linux-x86-64.so.2

find usr/lib/x86_64-linux-gnu/ -type l | while read F; 
do 
  readlink $F |grep -q '/lib/' && ln -sf ../../..$(readlink $F) $F 
done

ln -sf ../../../../../lib/x86_64-linux-gnu/libgcc_s.so.1 ./usr/lib/gcc/x86_64-linux-gnu/7/libgcc_s.so.1

find . -name 'lib*.so*'|xargs chmod +x
