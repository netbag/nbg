apt install --reinstall --download-only libmpfr6 libmpc3 \
libc-dev-bin binutils libc-bin linux-libc-dev libgmp10 \
gfortran-10 gcc-10 g++-10 libgomp1 zlib1g libisl22 libgcc1 \
libzstd1 libc6 libstdc++6 

# libcrypt-dev is not necessary, libssl1.1 libcrypt1

cp /var/cache/apt/archives/*.deb .
rm -f manpage* libcrypt-dev*

find ../sysdebs -type f|xargs -n 1 -i dpkg -x {} .

mv lib/libgcc_s.so.1 lib/x86_64-linux-gnu/
rm -rf usr/share/bug usr/share/man usr/share/doc

find . -type l |while read l; do readlink $l |grep -q '^/' && echo $l; done

ln -sf ..$(readlink lib64/ld-linux-x86-64.so.2) lib64/ld-linux-x86-64.so.2

find usr/lib/x86_64-linux-gnu/ -type l | while read F; 
do 
  readlink $F |grep -q '/lib/' && ln -sf ../../..$(readlink $F) $F 
done

find . -xtype l
