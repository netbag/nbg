apt install --reinstall --download-only libmpfr6 libmpc3 \
libc-dev-bin binutils libc-bin linux-libc-dev libgmp10 \
gfortran-10 gcc-10 g++-10 libgomp1 zlib1g libisl22 libgcc1 \
libzstd1 libc6 libstdc++6 

cp /var/cache/apt/archives/*.deb .

rm libcrypt-dev* manpage*

find ../sysdebs -type f|xargs -n 1 -i dpkg -x {} .

mv lib/libgcc_s.so.1 lib/aarch64-linux-gnu/

find usr/lib/aarch64-linux-gnu/ -type l | while read F; do 
   readlink $F |grep -q '/lib/' && ln -sf ../../..$(readlink $F) $F
done

