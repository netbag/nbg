rm -rf /var/cache/apt/archives/*

apt install --reinstall --download-only libmpfr6 libmpc3 libc-dev-bin binutils libc-bin linux-libc-dev libgmp10 gfortran-12 gcc-12 g++-12 libgomp1 zlib1g libisl23 libgcc-12-dev libzstd1 libnsl2 libstdc++6 libc6 libtirpc3 libcrypt1 libgcc-s1

ls /var/cache/apt/archives/
rm *font* *freetype* *gd* libj* *png* *tiff* *web* *xpm* man*

find ../*.deb -type f|xargs -n 1 -i dpkg -x {} .
rm -rf usr/share/bug usr/share/man usr/share/doc
find . -type l |xargs ls -l --color

: x86_64
ln -sf ..$(readlink lib64/ld-linux-x86-64.so.2) lib64/ld-linux-x86-64.so.2

find usr/lib/x86_64-linux-gnu/ -type l | while read F; 
do 
  readlink $F |grep -q '/lib/' && ln -sf ../../..$(readlink $F) $F 
done

: aarch64
find usr/lib/aarch64-linux-gnu/ -type l | while read F; 
do 
  readlink $F |grep -q '/lib/' && ln -sf ../../..$(readlink $F) $F 
done


find . -name 'lib*.so*'|xargs chmod +x
