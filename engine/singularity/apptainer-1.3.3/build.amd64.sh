mkdir rootfs
ipfs cat --progress=false QmZ6mXaUG4AUGpzKSMTaJCuX2w7XHgfr6MeWQt28HJdza8 | tar Jxf - -C rootfs
# go1.22.4.linux-amd64.tar.gz
ipfs cat --progress=false QmawhbKdh5grWeQbx2wkTPHtesFNSbp8pZ97rzZvwBMYcX | tar zxf - -C rootfs/opt/
# apptainer-1.3.3.tar.gz
ipfs cat --progress=false QmYrnhaDYwzhE4nbbBZ6kGr5pEM5BQCMMeHMp4iE4BVYbw | tar zxf - -C rootfs/root/
# squashfuse-0.5.2.tar.gz
ipfs cat --progress=false QmNNaGq3duT4HrV1tfs6pA3uWpFWQLaAmn8tQ5uTWaqPvi | tar zxf - -C rootfs/root/

echo nameserver 9.9.9.9 > rootfs/etc/resolv.conf
chroot rootfs sh -c 'apk add --no-progress musl-dev gcc bash xz-dev xz-static zstd-libs zstd-static fuse3-dev fuse3-static make linux-headers zlib-dev zlib-static lz4-dev lz4-static zstd-dev upx fuse-dev fuse-static sed'

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat <<'EOF' | chroot rootfs
export PATH=/opt/go/bin:$PATH
cd /root/apptainer-1.3.3
./mconfig --without-suid --without-network --without-seccomp --prefix=/opt/apptainer -P release-stripped -S -s
cd builddir
sed -i "/urandom/a GO_LDFLAGS += -ldflags \"-linkmode 'external' -extldflags '-static'\"" Makefile
make -j
make install
rm /opt/apptainer/bin/run-singularity
rm -rf /opt/apptainer/libexec/apptainer/lib/
strip /opt/apptainer/libexec/apptainer/bin/starter
upx   /opt/apptainer/libexec/apptainer/bin/starter
strip /opt/apptainer/bin/apptainer
upx   /opt/apptainer/bin/apptainer

cd /root/squashfuse-0.5.2/
./configure ; make 
gcc -Wall -g -O2 -o squashfuse_ll squashfuse_ll-ll_main.o  ./.libs/libsquashfuse_ll_convenience.a -llzma -lfuse3 -lpthread -lz -llz4 -lzstd -static
strip squashfuse_ll
upx squashfuse_ll
cp squashfuse_ll /opt/apptainer/bin/squashfuse3

apk del fuse3*

make clean
./configure ; make 
gcc -Wall -g -O2 -o squashfuse_ll squashfuse_ll-ll_main.o  ./.libs/libsquashfuse_ll_convenience.a -llzma -lfuse -lpthread -lz -llz4 -lzstd -static
strip squashfuse_ll
upx squashfuse_ll
cp squashfuse_ll /opt/apptainer/bin/squashfuse2

apk add fuse3-dev fuse3-static

cd /opt/apptainer/
rm -rf share
tar cf /apptainer.tar .
EOF

umount rootfs/dev
umount rootfs/proc
xz -9 -T 0 rootfs/apptainer.tar
mv rootfs/apptainer.tar.xz apptainer-1.3.3.$(arch).tar.xz
tar cf apptainer-1.3.3.buildenv.$(arch).tar -C rootfs .
xz -9 -T 0 apptainer-1.3.3.buildenv.$(arch).tar
