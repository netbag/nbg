#! /bin/bash

mkdir rootfs
ipfs cat --progress=false QmdhGh7bsgSUuY5GV2Y14jNNzAV3V4meC3xvNQ4Q9pRMLT | tar Jxf - -C rootfs
ipfs cat --progress=false QmZgyRghmArmJGvD2Z72M88SuLzNw8Pkyy59FqMct8CfqQ | tar zxf - -C rootfs/opt/
echo nameserver 9.9.9.9 > rootfs/etc/resolv.conf
ipfs cat --progress=false QmVxc6VkABVMEpYfU73AAUYTE9oH7czD3fEpzVXdNSS7Aw | tar zxf - -C rootfs/root/
ipfs cat --progress=false QmNNaGq3duT4HrV1tfs6pA3uWpFWQLaAmn8tQ5uTWaqPvi | tar zxf - -C rootfs/root/

chroot rootfs sh -c 'apk add --no-progress upx musl-dev gcc bash xz-dev xz-static zstd-libs zstd-static fuse3-dev fuse3-static make linux-headers zlib-dev zlib-static lz4-dev lz4-static zstd-dev binutils-gold fuse-dev fuse-static'

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat <<'EOF' | chroot rootfs
export PATH=/opt/go/bin:$PATH
cd /root/singularity-ce-4.1.4
./mconfig --without-squashfuse --without-seccomp --without-conmon --without-suid --without-network --prefix=/opt/singularity/4.1.3 -P release-stripped -S -s
cd builddir
sed -i "/GO_LDFLAGS +=/s:$: -ldflags \"-linkmode 'external' -extldflags '-static'\":" Makefile
make -j
make install
strip /opt/singularity/4.1.3/libexec/singularity/bin/starter
strip /opt/singularity/4.1.3/bin/singularity
upx /opt/singularity/4.1.3/libexec/singularity/bin/starter
upx /opt/singularity/4.1.3/bin/singularity
rm -rf /opt/singularity/4.1.3/libexec/singularity/bin/singularity-buildkitd
rm -rf /opt/singularity/4.1.3/bin/run-singularity

cd /root/squashfuse-0.5.2/
./configure ; make 
gcc -Wall -g -O2 -o squashfuse_ll squashfuse_ll-ll_main.o  ./.libs/libsquashfuse_ll_convenience.a -llzma -lfuse3 -lpthread -lz -llz4 -lzstd -static
strip squashfuse_ll
upx squashfuse_ll
cp squashfuse_ll /opt/singularity/4.1.3/bin/squashfuse3
make clean

apk del fuse3*
./configure ; make 
gcc -Wall -g -O2 -o squashfuse_ll squashfuse_ll-ll_main.o  ./.libs/libsquashfuse_ll_convenience.a -llzma -lfuse -lpthread -lz -llz4 -lzstd -static
strip squashfuse_ll
upx squashfuse_ll
cp squashfuse_ll /opt/singularity/4.1.3/bin/squashfuse2

apk add fuse3-dev fuse3-static

cd /opt/singularity/4.1.3
rm -rf share
tar cf /s.tar .
EOF

umount rootfs/dev
umount rootfs/proc
mv rootfs/s.tar .
