#! /bin/bash

mkdir rootfs
# alpine dev
ipfs cat --progress=false QmYv3ipPy3vfuhyUho93XKc1w4CMFBc3n2k5dAmNenc9Fm | tar Jxf - -C rootfs
# go
ipfs cat --progress=false QmawhbKdh5grWeQbx2wkTPHtesFNSbp8pZ97rzZvwBMYcX | tar zxf - -C rootfs/opt/
# singularity-ce-4.2.0.tar.gz
ipfs cat --progress=false QmaZPYgqh53JwKURumWB8atjLawzopVToEsiayu3rnNZ58 | tar zxf - -C rootfs/root/
# squashfuse
ipfs cat --progress=false QmNNaGq3duT4HrV1tfs6pA3uWpFWQLaAmn8tQ5uTWaqPvi | tar zxf - -C rootfs/root/

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat <<'EOF' | chroot rootfs
export PATH=/opt/go/bin:$PATH
cd /root/singularity-ce-4.2.0
./mconfig --without-squashfuse --without-seccomp --without-conmon --without-suid --without-network --prefix=/opt/singularity/4.2.0 -P release-stripped -S -s
cd builddir
sed -i "/GO_LDFLAGS +=/s:$: -ldflags \"-linkmode 'external' -extldflags '-static'\":" Makefile
make -j
make install
strip /opt/singularity/4.2.0/libexec/singularity/bin/starter
strip /opt/singularity/4.2.0/bin/singularity
upx /opt/singularity/4.2.0/libexec/singularity/bin/starter
upx /opt/singularity/4.2.0/bin/singularity
rm -rf /opt/singularity/4.2.0/libexec/singularity/bin/singularity-buildkitd
rm -rf /opt/singularity/4.2.0/bin/run-singularity

cd /root/squashfuse-0.5.2/
./configure ; make 
gcc -Wall -g -O2 -o squashfuse_ll squashfuse_ll-ll_main.o  ./.libs/libsquashfuse_ll_convenience.a -llzma -lfuse3 -lpthread -lz -llz4 -lzstd -static
strip squashfuse_ll
upx squashfuse_ll
cp squashfuse_ll /opt/singularity/4.2.0/bin/squashfuse3

apk del fuse3*

make clean
./configure ; make 
gcc -Wall -g -O2 -o squashfuse_ll squashfuse_ll-ll_main.o  ./.libs/libsquashfuse_ll_convenience.a -llzma -lfuse -lpthread -lz -llz4 -lzstd -static
strip squashfuse_ll
upx squashfuse_ll
cp squashfuse_ll /opt/singularity/4.2.0/bin/squashfuse2

cd /opt/singularity/4.2.0
rm -rf share
tar cf /s.tar .
EOF

umount rootfs/dev
umount rootfs/proc
mv rootfs/s.tar .
