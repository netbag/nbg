mkdir -p tmpl/.bin
# QmNWrtMn7NUH7KzYHXfFZUZSJHFGmTZJQV56KWWGfTWpMS busybox.amd64
ipfs cat --progress=false QmNWrtMn7NUH7KzYHXfFZUZSJHFGmTZJQV56KWWGfTWpMS > tmpl/.bin/busybox
# Qme8V4VHkqn4yHfU8PsS1GMJ9NpuEKTXWnvwEpBRiUxZik singularity.amd64.tar.xz
ipfs cat --progress=false Qme8V4VHkqn4yHfU8PsS1GMJ9NpuEKTXWnvwEpBRiUxZik | tar Jxf - -C tmpl

chmod +x tmpl/.bin/busybox
ln -sf busybox tmpl/.bin/sh
ln -sf busybox tmpl/.bin/which
ln -sf busybox tmpl/.bin/dirname
ln -sf busybox tmpl/.bin/realpath
ln -sf busybox tmpl/.bin/readlink

cat <<'EOF' > tmpl/tobe.run
a="/$0"; a="${a%/*}"; a="${a:-.}"; a="${a##/}/"
DIR=$(${a}.bin/realpath ${a})
export PATH=$DIR/bin:$PATH:$DIR/.bin
which fusermount &> /dev/null && ln -sf squashfuse2 $DIR/bin/squashfuse
which fusermount3 &> /dev/null && ln -sf squashfuse3 $DIR/bin/squashfuse
$DIR/.bin/sh $DIR/run.sh "$@"
EOF
