export PATH=/data/musl/aarch64-notaos-linux-musl/bin:/data/musl/x86_64-notaos-linux-musl/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

rm -rf zstd-1.5.6; tar xf src/zstd-1.5.6.tar.gz
(
cd zstd-1.5.6
CC=x86_64-notaos-linux-musl-gcc make -j 
cp -a lib /tmp/zstd.lib.amd64
)
rm -rf zstd-1.5.6

rm -rf libarchive-3.7.4; tar xf src/libarchive-3.7.4.tar.xz
(
cd libarchive-3.7.4
./configure --host=x86_64-notaos-linux-musl LDFLAGS='-L/tmp/zstd.lib.amd64 -static --static' CFLAGS="-I/tmp/zstd.lib.amd64"
make -j
make DESTDIR=/tmp/libarchive.amd64 install
)
rm -rf libarchive-3.7.4

x86_64-notaos-linux-musl-gcc -o header.amd64 -I/tmp/libarchive.amd64/usr/local/include -L/tmp/libarchive.amd64/usr/local/lib -L/tmp/zstd.lib.amd64 header.c -larchive -lzstd -static
x86_64-notaos-linux-musl-gcc -o netbag.amd64 -I/tmp/libarchive.amd64/usr/local/include -L/tmp/libarchive.amd64/usr/local/lib -L/tmp/zstd.lib.amd64 netbag.c -larchive -lzstd -static

rm -rf /tmp/libarchive.amd64/ /tmp/zstd.lib.amd64
x86_64-notaos-linux-musl-strip header.amd64
x86_64-notaos-linux-musl-strip netbag.amd64
truncate --size=2M header.amd64
truncate --size=2M netbag.amd64
mv header.amd64 bin/
mv netbag.amd64 bin/

rm -rf zstd-1.5.6; tar xf src/zstd-1.5.6.tar.gz
(
cd zstd-1.5.6
CC=aarch64-notaos-linux-musl-gcc make -j 
cp -a lib /tmp/zstd.lib.arm64
)
rm -rf zstd-1.5.6

rm -rf libarchive-3.7.4; tar xf src/libarchive-3.7.4.tar.xz
(
cd libarchive-3.7.4
./configure --host=aarch64-notaos-linux-musl LDFLAGS='-L/tmp/zstd.lib.arm64 -static --static' CFLAGS="-I/tmp/zstd.lib.arm64"
make -j
make DESTDIR=/tmp/libarchive.arm64 install
)
rm -rf libarchive-3.7.4

aarch64-notaos-linux-musl-gcc -o header.arm64 -I/tmp/libarchive.arm64/usr/local/include -L/tmp/libarchive.arm64/usr/local/lib -L/tmp/zstd.lib.arm64 header.c -larchive -lzstd -static
aarch64-notaos-linux-musl-gcc -o netbag.arm64 -I/tmp/libarchive.arm64/usr/local/include -L/tmp/libarchive.arm64/usr/local/lib -L/tmp/zstd.lib.arm64 netbag.c -larchive -lzstd -static

rm -rf /tmp/libarchive.arm64/ /tmp/zstd.lib.arm64
aarch64-notaos-linux-musl-strip header.arm64
aarch64-notaos-linux-musl-strip netbag.arm64
truncate --size=2M header.arm64
truncate --size=2M netbag.arm64
mv header.arm64 bin/
mv netbag.arm64 bin/

rm -rf gawk-5.2.2; tar xf src/gawk-5.2.2.tar.xz
(
cd gawk-5.2.2
LDFLAGS='-static' ./configure --host=x86_64-notaos-linux-musl 
make -j
x86_64-notaos-linux-musl-strip gawk
mv gawk ../bin/gawk.amd64
)

rm -rf gawk-5.2.2; tar xf src/gawk-5.2.2.tar.xz
(
cd gawk-5.2.2
LDFLAGS='-static' ./configure --host=aarch64-notaos-linux-musl 
make -j
aarch64-notaos-linux-musl-strip gawk
mv gawk ../bin/gawk.arm64
)
rm -rf gawk-5.2.2

rm -rf m4-1.4.19 ; tar xf src/m4-1.4.19.tar.bz2
(
cd m4-1.4.19
LDFLAGS='-static' ./configure --host=x86_64-notaos-linux-musl 
make -j
x86_64-notaos-linux-musl-strip src/m4
mv src/m4 ../bin/m4.amd64
)

rm -rf m4-1.4.19 ; tar xf src/m4-1.4.19.tar.bz2
(
cd m4-1.4.19
LDFLAGS='-static' ./configure --host=aarch64-notaos-linux-musl 
make -j
aarch64-notaos-linux-musl-strip src/m4
mv src/m4 ../bin/m4.arm64
)
rm -rf m4-1.4.19

rm -rf bash-5.1.16 ; tar xf src/bash-5.1.16.tar.gz
(
cd bash-5.1.16
LDFLAGS='-static' ./configure --host=x86_64-notaos-linux-musl --enable-static-link --without-bash-malloc
make -j
x86_64-notaos-linux-musl-strip bash
mv bash ../bin/bash.amd64
)

rm -rf bash-5.1.16 ; tar xf src/bash-5.1.16.tar.gz
(
cd bash-5.1.16
LDFLAGS='-static' ./configure --host=aarch64-notaos-linux-musl --enable-static-link --without-bash-malloc
make -j
aarch64-notaos-linux-musl-strip bash
mv bash ../bin/bash.arm64
)
rm -rf bash-5.1.16

rm -rf ninja-1.12.1 ; tar xf src/ninja-1.12.1.tar.gz
(
cd ninja-1.12.1
CXX=x86_64-notaos-linux-musl-c++ AR=x86_64-notaos-linux-musl-ar LDFLAGS='-static' ./configure.py
ninja
x86_64-notaos-linux-musl-strip ninja
mv ninja ../bin/ninja.amd64
)
rm -rf ninja-1.12.1 ; tar xf src/ninja-1.12.1.tar.gz
(
cd ninja-1.12.1
CXX=aarch64-notaos-linux-musl-c++ AR=aarch64-notaos-linux-musl-ar LDFLAGS='-static' ./configure.py
ninja
aarch64-notaos-linux-musl-strip ninja
mv ninja ../bin/ninja.arm64
)
rm -rf ninja-1.12.1

rm -rf make-4.4.1 ; tar xf src/make-4.4.1.tar.gz
(
cd make-4.4.1
LDFLAGS='-static' ./configure --host=x86_64-notaos-linux-musl
make -j
x86_64-notaos-linux-musl-strip make
mv make ../bin/make.amd64
)
rm -rf make-4.4.1 ; tar xf src/make-4.4.1.tar.gz
(
cd make-4.4.1
LDFLAGS='-static' ./configure --host=aarch64-notaos-linux-musl
make -j
aarch64-notaos-linux-musl-strip make
mv make ../bin/make.arm64
)
rm -rf make-4.4.1

rm -rf busybox-1.36.1; tar xf src/busybox-1.36.1.tar.bz2 
(
cd busybox-1.36.1
make CROSS_COMPILE=x86_64-notaos-linux-musl- defconfig
sed 's/^.*CONFIG_STATIC .*$/CONFIG_STATIC=y/' -i .config
sed 's/^.*CONFIG_FEATURE_MODPROBE.*$/CONFIG_FEATURE_MODPROBE_BLACKLIST=y/' -i .config
sed 's/^.*CONFIG_MODPROBE_SMALL.*$/# CONFIG_MODPROBE_SMALL is not set/' -i .config
sed 's/^.*CONFIG_FEATURE_MODUTILS_ALIAS.*$/CONFIG_FEATURE_MODUTILS_ALIAS=y/' -i .config 
make CROSS_COMPILE=x86_64-notaos-linux-musl- -j
mv busybox ../bin/busybox.amd64
)
rm -rf busybox-1.36.1; tar xf src/busybox-1.36.1.tar.bz2 
(
cd busybox-1.36.1
make CROSS_COMPILE=aarch64-notaos-linux-musl- defconfig
sed 's/^.*CONFIG_STATIC .*$/CONFIG_STATIC=y/' -i .config
sed 's/^.*CONFIG_FEATURE_MODPROBE.*$/CONFIG_FEATURE_MODPROBE_BLACKLIST=y/' -i .config
sed 's/^.*CONFIG_MODPROBE_SMALL.*$/# CONFIG_MODPROBE_SMALL is not set/' -i .config
sed 's/^.*CONFIG_FEATURE_MODUTILS_ALIAS.*$/CONFIG_FEATURE_MODUTILS_ALIAS=y/' -i .config 
make CROSS_COMPILE=aarch64-notaos-linux-musl- -j
mv busybox ../bin/busybox.arm64
)
rm -rf busybox-1.36.1

upx bin/b* bin/g* bin/m* bin/ninja*
