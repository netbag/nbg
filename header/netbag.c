#include <sys/types.h>
#include <sys/stat.h>
#include <archive.h>
#include <archive_entry.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <linux/limits.h>
#include <libgen.h>

#define SKIPSIZE (1024 * 1024 * 2)

static int copy_data(struct archive *ar, struct archive *aw)
{
	int r;
	const void *buff;
	size_t size;
	int64_t offset;

	for (;;) {
		r = archive_read_data_block(ar, &buff, &size, &offset);
		if (r == ARCHIVE_EOF)
			return (ARCHIVE_OK);
		if (r != ARCHIVE_OK)
			return (r);
		r = archive_write_data_block(aw, buff, size, offset);
		if (r != ARCHIVE_OK) {
			printf("%s", archive_error_string(aw));
			return (r);
		}
	}
}

int main(int argc, char *argv[])
{
	struct archive *a;
	struct archive *ext;
	struct archive_entry *entry;
	char exe[PATH_MAX] = { '\0' };
	char dir[PATH_MAX] = { '\0' };
	char repo[PATH_MAX] = { '\0' };
	char *dest, **new_argv;
	struct stat st = { 0 };
	int r;
	FILE *f;

	if (!getenv("NETBAG_REPO")) {
		if (getenv("HOME")) {
			snprintf(repo, PATH_MAX, "%s/.netbag", getenv("HOME"));
			mkdir(repo, 0755);
		} else {
			printf
			    ("Error getting repo, set $HOME or $NETBAG_REPO first.\n");
			exit(1);
		}
	} else
		snprintf(repo, PATH_MAX, "%s", getenv("NETBAG_REPO"));

	if ((r = readlink("/proc/self/exe", exe, PATH_MAX)) < 0)
		exit(1);

	f = fopen(exe, "rb");
	fseek(f, SKIPSIZE, SEEK_SET);

	a = archive_read_new();
	archive_read_support_filter_all(a);
	archive_read_support_format_all(a);
	r = archive_read_open_FILE(a, f);
	if (r != ARCHIVE_OK)
		exit(1);

	while (archive_read_next_header(a, &entry) == ARCHIVE_OK) {
		if (strstr(archive_entry_pathname(entry), "/tobe.run")) {
			snprintf(dir, PATH_MAX, "%s", dirname((char *)
							      archive_entry_pathname(entry)));
			break;
		}
	}
	archive_read_close(a);
	archive_read_free(a);

	new_argv = malloc((argc + 2) * sizeof *new_argv);

	asprintf(&new_argv[0], "%s/%s/.bin/sh", repo, dir);
	asprintf(&new_argv[1], "%s/%s/tobe.run", repo, dir);

	for (int i = 1; i < argc; ++i) {
		size_t length = strlen(argv[i]) + 1;
		new_argv[i + 1] = malloc(length);
		memcpy(new_argv[i + 1], argv[i], length);
		new_argv[i + 1][length] = '\0';
	}
	new_argv[argc + 1] = NULL;

	if (stat(new_argv[1], &st) == -1) {
		fseek(f, SKIPSIZE, SEEK_SET);
		a = archive_read_new();
		ext = archive_write_disk_new();
		archive_write_disk_set_options(ext, ARCHIVE_EXTRACT_TIME);
		archive_read_support_filter_all(a);
		archive_read_support_format_all(a);

		r = archive_read_open_FILE(a, f);
		if (r != ARCHIVE_OK) {
			printf("Error opening archive\n");
			exit(1);
		}

		for (;;) {
			r = archive_read_next_header(a, &entry);
			if (r == ARCHIVE_EOF)
				break;
			if (r != ARCHIVE_OK)
				printf("%s %d\n", archive_error_string(a), 1);

			asprintf(&dest, "%s/%s", repo,
				 archive_entry_pathname(entry));
			archive_entry_set_pathname(entry, dest);

			r = archive_write_header(ext, entry);
			if (r != ARCHIVE_OK)
				printf("%s \n", archive_error_string(ext));
			else {
				copy_data(a, ext);
				r = archive_write_finish_entry(ext);
				if (r != ARCHIVE_OK)
					printf("%s %d",
					       archive_error_string(ext), 1);
			}
			free(dest);
		}

		archive_read_close(a);
		archive_read_free(a);
	}
	fclose(f);

	execv(new_argv[0], new_argv);
	_exit(EXIT_FAILURE);
}
