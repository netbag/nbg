recipe from : https://www.amd.com/de/developer/zen-software-studio/applications/spack/hpc-applications-openfoam.html

```
# Installation of AOCC
$ spack install aocc +license-agreed

# The compiler can be added to spack in two ways
# Add the installed compiler by providing the path
$ spack cd -i aocc
$ spack compiler add $PWD

# Add the installed compiler from the environment
$ spack load aocc
$ spack compiler find
```

```
# Example for building OpenFOAM with AOCC and AOCL.
$ spack install openfoam %aocc ^amdfftw ^openmpi fabrics=auto
```

```
## Loading OpenFOAM build with AOCC
spack load openfoam

# Motorbike dataset is available at $(spack location -i openfoam)/tutorials/incompressible/simpleFoam/motorBike
cd $(spack location -i openfoam )/tutorials/incompressible/simpleFoam/motorBike

# NP=Number of cores available in the system.
export NP=$(nproc)

# Create decomposeParDict file to support $NP number of processes.
cp system/decomposeParDict.6 system/decomposeParDict.${NP}

# Change the numberOfSubdomains in decomposeParDict to create $NP number of subdomains
sed -i "s/numberOfSubdomains.*/numberOfSubdomains ${NP};/" system/decomposeParDict.${NP}

#Change the grid size for $NP, here setting grid size for 192 core system (48*4*1)
sed -i "s/ n           (3 2 1)/ n           (48 4 1);/" system/decomposeParDict.${NP}

#Default Mesh size is very small (0.35M cells), scaling up the Mesh size to 20M cells
sed -i "s/(20 8 8)/(100 40 40)/" system/blockMeshDict

#To use the file "system/decomposeParDict.${NP}" in Allrun (run script)
sed -i "s#system/decomposeParDict.6#system/decomposeParDict.${NP}#" Allrun

#To run the benchmark
#Allrun script executes a combination multiple steps
#like Mesh creation, Setting the initial fields, Running the application, Postprocessing etc.
./Allrun
```
