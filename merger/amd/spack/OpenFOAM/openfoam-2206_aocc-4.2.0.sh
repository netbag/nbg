mkdir -p rootfs
mount -t tmpfs none rootfs
# QmYCiRErugRN5rMLvJvyRJvNBeZwKoKf9xX5dRnfoY8AK7 aocc-4.2.0_spack-0.22.0_almalinux-8.8.nocache.tar.xz
ipfs cat --progress=false QmYCiRErugRN5rMLvJvyRJvNBeZwKoKf9xX5dRnfoY8AK7 | tar Jxf - -C rootfs

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat << 'EOF' | chroot rootfs
export PATH=/opt/spack/bin:$PATH
spack install -j 128 openfoam@2206%aocc target=cascadelake ^amdfftw ^openmpi fabrics=ucx,verbs ^rdma-core~pyverbs~man_pages
yes | spack module tcl refresh
EOF

umount rootfs/proc
umount rootfs/dev

cat $0 >> rootfs/.nbg
rm -rf rootfs/opt/spack/var/spack/cache/_source-cache/archive/*
tar cf openfoam-2206_aocc-4.2.0.spack.cascadelake.tar -C rootfs .
xz -9 -T 0 openfoam-2206_aocc-4.2.0.spack.cascadelake.tar
umount rootfs
