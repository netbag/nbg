Ubuntu base rootfs from https://cdimage.ubuntu.com/ubuntu-base/releases/jammy/release/

```
QmPhm12VMUEXqLGZjLedyLgUrEgNuUAES1Z2qtDgqKya7x ubuntu-base-22.04.4-base-amd64.tar.gz
QmXwVXPrehGHRp5NxTBhLWdYwEKkmbSbkCVnvroT332qUL ubuntu-base-22.04.4-base-arm64.tar.gz
Qmeqrx5vU3XLVaF6hcjsYZhqURi4MbgoheRmrcyWk5uYv1 ubuntu-base-22.04.4-base-armhf.tar.gz
QmfGBqSP8rh4kT2cD8hJNq4ZkjwAqxQeSu9Mcobu6kw66a ubuntu-base-22.04.4-base-ppc64el.tar.gz
QmPKM2eLJZvPJKGCoUyzdE9S3tEVYYZLYci7j3eJCLJiD3 ubuntu-base-22.04.4-base-riscv64.tar.gz
QmUQAysxaf4p1SbvRX6EwMcwKvCPYoozoHwNVQBBAEmEQi ubuntu-base-22.04.4-base-s390x.tar.gz
```
```
QmaqKWDbymY6TDdq2Gpngog39FZw7qcM3dvr3Nq3emUjy1 Miniconda3-py310_24.5.0-0-Linux-aarch64.sh
QmPS5jiU7ZuB9ghJirgo6Y3zvt9kj5hxUpfbbs6eAD7eAb Miniconda3-py310_24.5.0-0-Linux-x86_64.sh
```
