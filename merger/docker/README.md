# Generate rootfs from docker registry

## with singularity
```
url="docker://quay.io/biocontainers/aacon:1.1--hdfd78af_0"
singularity build --sandbox rootfs $url
rm -rf rootfs/.test rootfs/.shell rootfs/.run rootfs/.exec rootfs/environment rootfs/.singularity.d rootfs/singularity
cat <<EOF > rootfs/.nbg
AUTHOR: @netbag
SOURCE: $url
EOF
tar cf r.tar -C rootfs .
zstd -7 r.tar
```

## with skopeo + undocker

```
skopeo copy docker://docker.io/busybox:latest docker-archive:busybox.docker.tar
undocker busybox.docker.tar busybox.rootfs.tar
# singularity build --sandbox rootfs docker-archive:busybox.docker.tar
```

static linking skopeo:
```
env GOOS=linux GOARCH=amd64 CGO_ENABLED=1 go build "-buildmode=pie" -ldflags '-X main.gitCommit= ' -gcflags "" -tags "btrfs_noversion exclude_graphdriver_devicemapper exclude_graphdriver_btrfs  containers_image_openpgp" -ldflags "-linkmode 'external' -extldflags '-static'" -o bin/skopeo.amd64 ./cmd/skopeo
```
```
env GOOS=linux GOARCH=arm64 CGO_ENABLED=1 go build "-buildmode=pie" -ldflags '-X main.gitCommit= ' -gcflags "" -tags "btrfs_noversion exclude_graphdriver_btrfs exclude_graphdriver_devicemapper containers_image_openpgp" -ldflags "-linkmode 'external' -extldflags '-static'" -o bin/skopeo.arm64 ./cmd/skopeo

```
static linking undocker:
```
env GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build 
env GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build 
```

**static linked binaries:**
https://gateway.pinata.cloud/ipfs/QmZz4hrazCtpuDzWBbHS5co5saGtx7SC624rx2qbgujaFb/

ifps cids:
```
QmSE2oVBG8vQoxaXjwLrDDyBTFnJDTpuavoGWp6se2zHwz skopeo/skopeo-1.15.2.tar.gz
QmaVTDkCSpRmJ2wvBZvAgT5VTHFpz4YPnrkkHrH8Y854bS skopeo/skopeo.amd64
QmaqLMq9oNDxMKXMAoAaCF1adAeDw8yEdkVE5FG9Vkq7F2 skopeo/skopeo.arm64
QmNhCdydCFuuHWSiSqbNwp47nn3rMqyHCo1SgNG86d1uoF skopeo/undocker.amd64
QmYJmQrxYDKa7eyvYnYiA5nJ8SeeyDWJK3z1edAYrw9jyZ skopeo/undocker.arm64
QmUkfrofFNZfRHc82Ydow9ySHKbdsNY86SvwRvW7XkUttH skopeo/undocker.tar.gz
QmZz4hrazCtpuDzWBbHS5co5saGtx7SC624rx2qbgujaFb skopeo
```

patch for undocker
```
--- undocker/rootfs/rootfs.go
+++ rootfs/rootfs.go
@@ -78,7 +78,8 @@
 	}
 
 	// enumerate layers the way they would be laid down in the image
-	layers := make([]nameOffset, len(layerOffsets))
+	// layers := make([]nameOffset, len(layerOffsets))
+	layers := make([]nameOffset, len(manifest[0].Layers))
 	for i, name := range manifest[0].Layers {
 		layers[i] = nameOffset{
 			name:   name,
```
