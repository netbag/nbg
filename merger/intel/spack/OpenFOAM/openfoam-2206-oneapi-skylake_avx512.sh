mkdir -p rootfs
mount -t tmpfs none rootfs
#QmWZDha8AEpQazhdtt9xHLpcSYT6mTudiVNfXdt6c6QfgC oneapi-2024.1.0_spack-0.22.0_almalinux-8.8.nocache.tar.xz
ipfs cat --progress=false QmWZDha8AEpQazhdtt9xHLpcSYT6mTudiVNfXdt6c6QfgC | tar Jxf - -C rootfs

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat << 'EOF' | chroot rootfs
export PATH=/opt/spack/bin:$PATH
spack install -j 128 openfoam@2206%oneapi ^boost%gcc ^cgal%gcc ^intel-oneapi-mpi ^libfabric fabrics=sockets,tcp,udp,verbs ^rdma-core~pyverbs~man_pages target=skylake_avx512
yes | spack module tcl refresh
EOF

umount rootfs/proc
umount rootfs/dev

cat $0 >> rootfs/.nbg
rm -rf rootfs/opt/spack/var/spack/cache/_source-cache/archive/*
tar cf openfoam-2206_oneapi-2024.1.0.spack.tar -C rootfs .
xz -9 -T 0 openfoam-2206_oneapi-2024.1.0.spack.tar
umount rootfs
