# build gist

```sh
# https://www.cpan.org/src/5.0/perl-5.38.2.tar.gz   
# QmPAJ4MBAiPxpd4eDAztoxBh1ut1cvvfv5283FbTyfMzVf
unset LANG
sh Configure -des -Dprefix=INSTALL_PATH -Uloclibpth -Ulocincpth -Dcc=gcc
```
```sh
# https://fossies.org/linux/misc/bzip2-1.0.8.tar.gz
# QmTurG9rKw3R2zyGQxuPrWq1BP75SYrwwoMqMb87V6ThSR
make
make install PREFIX=INSTALL_PREFIX
make clean
make -f Makefile-libbz2_so
cp *.so.* INSTALL_PREFIX/lib/
```
