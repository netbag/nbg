#! /bin/bash

#https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-4.1.6.tar.bz2
#https://linux.mellanox.com/public/repo/mlnx_ofed/latest/SRPMS/ucx-1.17.0.tar.gz
#https://sources.buildroot.net/rdma-core/rdma-core-52.0.tar.gz
#https://sources.buildroot.net/libnl/libnl-3.9.0.tar.gz

#QmaHmZnMzA4HPKEuqRw8cy1i54D1abejHzdSiB5Yyoe9fB libnl-3.9.0.tar.gz
#QmfQAY4oDCypPF3UeRmH6Latfm9fTZsjJpF5R76SZ7ZQdB openmpi-4.1.6.tar.bz2
#QmWZFJzAiZufmq5PMvHKxcwTRXcGhMx3Mr8EihZesKC5Pd rdma-core-52.0.tar.gz
#QmcrLikk7jxW9tLNKzHnCPrZkeqhaJUmesX18bSWZ5p3V2 ucx-1.17.0.tar.gz

curl https://get.hpc.dev/r/x.tar.xz | tar Jxf -
chroot . sh -c 'echo nameserver 9.9.9.9 > /etc/resolv.conf; apk add --no-progress bash make gawk sed cmake make patch git curl wget perl python3 pkgconfig flex bison'

ls *.tar |xargs -n 1 tar xf
export PREFIX=/opt/netbag/openmpi
mkdir -p $PREFIX/lib
ln -sf lib $PREFIX/lib64
export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig
export LD_LIBRARY_PATH=$PREFIX/lib
export CPPFLAGS="-I$PREFIX/include"
export LDFLAGS="-L$PREFIX/lib"
(cd libnl-3.9.0; ./configure --prefix=$PREFIX --disable-debug; make -j; make install)
(cd rdma-core-52.0; mkdir build; cd build; cmake .. -DNO_MAN_PAGES=1 -DCMAKE_BUILD_TYPE=Release; make -j; cmake --install . --prefix $PREFIX)
(cd ucx-1.17.0; ./configure --with-verbs=$PREFIX --prefix=$PREFIX --with-rc --with-ud --with-dc; make -j; make install)
(cd openmpi-4.1.6; ./configure --with-ucx=$PREFIX --enable-mca-no-build=btl-uct --prefix=$PREFIX --with-verbs=$PREFIX; make -j; make install)
