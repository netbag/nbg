#! /bin/bash
# QmTurG9rKw3R2zyGQxuPrWq1BP75SYrwwoMqMb87V6ThSR bzip2-1.0.8.tar.gz
# QmdVs1fPNd2xh5oyCxQeE5AJX39q1RkxrF6XDKoJg3D4r2 libffi-3.4.5.tar.gz
# QmXuWRTUZwTbip7rJYzjB69S5dATkREaAwDCQcSkoBMtsh ncurses-6.5.tar.gz
# QmNey44BfKyKEQC51n5QnAPRTFL3gzVNFtQDzpuAevd9QS openssl-1.1.1w.tar.gz
# Qmb3DQh9CPfnsoPZs46vetY76VwthTFoX6ijX8wbSvZFwY pyenv-2.4.10.tar.gz
# QmeMAthg5awfw5qL9YtLUywuPXpM4hwS99DGZr9YALQYiP readline-8.2.13.tar.gz
# QmYcernyaDgq8n739kwzZKiMgBZzNpzAqd6eWyuUhgAe8N sqlite-autoconf-3460000.tar.gz
# QmUtf3XxrruXvVth5BVSyh7e8935eCoXdaT7j88pE8BFm6 xz-5.6.2.tar.gz
# QmWXbiLEDwfLgYkxtLyME1cdCYcCgyDfPFyjT2U6GVCYam zlib-1.3.1.tar.gz

ls *.tar.* | xargs -n 1 tar xf

# env -i /bin/bash

export PREFIX=/+/aux
mkdir -p $PREFIX
mv pyenv-2.4.10 $PREFIX/pyenv

(cd $PREFIX; mkdir lib; ln -sf lib lib64)
export PYENV_ROOT=$PREFIX/pyenv
export PATH=$PREFIX/bin:$PREFIX/pyenv/bin:$PATH
eval "$(pyenv init -)"

cat<<EOF > $PREFIX/env.sh
export PYENV_ROOT=$PREFIX/pyenv          
export PATH=$PREFIX/bin:$PREFIX/pyenv/bin:\$PATH      
export LD_LIBRARY_PATH=$PREFIX/lib:\$LD_LIBRARY_PATH
eval "\$(pyenv init -)" 
EOF

export CFLAGS="-I$PREFIX/include"
export CPPFLAGS="-I$PREFIX/include"
export LDFLAGS="-L$PREFIX/lib"
export LD_LIBRARY_PATH=$PREFIX/lib:$LD_LIBRARY_PATH

(cd zlib-1.3.1; ./configure --prefix=$PREFIX; make -j; make install)
(cd bzip2-1.0.8; make ; make install PREFIX=$PREFIX; make clean; make -f Makefile-libbz2_so; cp *.so* $PREFIX/lib; ln -sf libbz2.so.1.0 $PREFIX/lib/libbz2.so)
(cd xz-5.6.2; ./configure --prefix=$PREFIX --disable-static; make -j; make install)
(cd libffi-3.4.5; ./configure --prefix=$PREFIX --disable-docs; make -j; make install)
(cd ncurses-6.5; ./configure --prefix=$PREFIX  --without-manpages --with-shared --enable-termcap --disable-widec --enable-overwrite; make -j; make install )
(cd readline-8.2.13; ./configure --prefix=$PREFIX  --with-curses; make SHLIB_LIBS="-lncurses"; make SHLIB_LIBS="-lncurses" install)
(cd openssl-1.1.1w; ./config --prefix=$PREFIX --openssldir=$PREFIX/etc/ssl --libdir=lib shared; make install_sw)
(cd sqlite-autoconf-3460000; ./configure --prefix=$PREFIX --disable-static  --enable-fts{4,5}  CPPFLAGS="-D SQLITE_ENABLE_COLUMN_METADATA=1 -D SQLITE_ENABLE_UNLOCK_NOTIFY=1 -D SQLITE_ENABLE_DBSTAT_VTAB=1 -D SQLITE_SECURE_DELETE=1";  make -j; make install) 

export CONFIGURE_OPTS="--with-openssl=$PREFIX"
pyenv install 3.10.10
