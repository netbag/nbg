mkdir -p rootfs
mount -t tmpfs none rootfs
# almalinux 8.8 with gcc aarch64
ipfs cat --progress=false QmRPfci8NpK3onFvyEXn5np4cA7MRQX2Wsd3WwhSAGbcjT |tar Jxf - -C rootfs
# spack 0.22.0
ipfs cat --progress=false Qmd799ZryHUBDjB1QBp3MEPTiRbau55mJKbQr7jtKGXMT8 |tar zxf - -C rootfs/opt/

mv rootfs/opt/spack-0.22.0 rootfs/opt/spack
CONF=rootfs/opt/spack/etc/spack/defaults/config.yaml
sed -i 's:$spack/opt/spack:/opt/nbg:' $CONF
sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}_b{hash:1};' $CONF
##sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}/{hash:3};' $CONF

echo 'nameserver 9.9.9.9' > rootfs/etc/resolv.conf

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat << 'EOF' | chroot rootfs
export PATH=/opt/spack/bin:$PATH
spack compiler add
spack install -j 64 gcc@12.3.0
spack compiler add `spack location -i gcc@12.3.0`
spack install -j 64 wrf@4.5.1 %gcc@12.3.0 ^openmpi fabrics=verbs,ofi,ucx ^ucx+verbs+rc+ud+rdmacm+numa
yes | spack module tcl refresh
EOF

umount rootfs/proc
umount rootfs/dev

rm -rf rootfs/tmp/*
cat $0 >> rootfs/.nbg
tar cf wrf-4.5.1_gcc-12.3.0.spack.aarch64.tar -C rootfs .
xz -9 -T 0 wrf-4.5.1_gcc-12.3.0.spack.aarch64.tar
umount rootfs
rmdir rootfs
