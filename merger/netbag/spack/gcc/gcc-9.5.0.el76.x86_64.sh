mkdir -p rootfs
mount -t tmpfs none rootfs
# QmU1duDY6h5rr3xFSyw6KHcp9JBULQwqznyiaeriDXFsvt spack-0.22.1_el76.tar.xz
ipfs cat --progress=false QmU1duDY6h5rr3xFSyw6KHcp9JBULQwqznyiaeriDXFsvt |tar Jxf - -C rootfs

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat << 'EOF' | chroot rootfs
sed -i 's:$spack/opt/spack:/opt/nbg/gcc-9.5.0:' /opt/spack/etc/spack/defaults/config.yaml
sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};.pkg/{name}/{version};' /opt/spack/etc/spack/defaults/config.yaml
export PATH=/opt/spack/bin:$PATH
spack compiler add
# https://savannah.gnu.org/bugs/?65811
# gettext 0.22 build error
sed -i '/pkgversion/s/Spack GCC/Netbag Spack GCC/' /opt/spack/var/spack/repos/builtin/packages/gcc/package.py
sed -i '/bugurl/s;https://github.com/spack/spack/issues;https://gitlab.com/netbag/;' /opt/spack/var/spack/repos/builtin/packages/gcc/package.py
spack install gcc@9.5.0 ^gettext@0.21
yes | spack module tcl refresh
EOF

umount rootfs/proc
umount rootfs/dev

rm -rf rootfs/tmp/*
cat $0 >> rootfs/.nbg
rm -rf rootfs/opt/spack/var/spack/cache/_source-cache/archive/*
tar cf gcc-9.5.0.el76.tar -C rootfs .
xz -9 -T 0 gcc-9.5.0.el76.tar
umount rootfs
rmdir rootfs
