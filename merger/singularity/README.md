Get rootfs from singularity image.

```
SIF=app.sif
unsquashfs -o $(grep --byte-offset --only-matching --text hsqs $SIF | cut -d ':' -f 1) $SIF
```
