# A flexible package manager that supports multiple versions, configurations, platforms, and compilers.
      
https://spack.readthedocs.io/en/latest/
     
```
$ spack arch --known-targets
Generic architectures (families)
    aarch64  armv8.1a  armv8.3a  armv8.5a  ppc    ppc64le  riscv64  sparc64  x86_64     x86_64_v3
    arm      armv8.2a  armv8.4a  armv9.0a  ppc64  ppcle    sparc    x86      x86_64_v2  x86_64_v4

GenuineIntel - x86
    i686  pentium2  pentium3  pentium4  prescott

GenuineIntel - x86_64
    nocona  nehalem   sandybridge  haswell    skylake  cannonlake      cascadelake  sapphirerapids
    core2   westmere  ivybridge    broadwell  mic_knl  skylake_avx512  icelake

AuthenticAMD - x86_64
    k10  bulldozer  piledriver  zen  steamroller  zen2  zen3  excavator  zen4

IBM - ppc64
    power7  power8  power9  power10

IBM - ppc64le
    power8le  power9le  power10le

Cavium - aarch64
    thunderx2

Fujitsu - aarch64
    a64fx

ARM - aarch64
    cortex_a72  neoverse_n1  neoverse_v1  neoverse_v2

Apple - aarch64
    m1  m2

SiFive - riscv64
    u74mc
```
