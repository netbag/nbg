mkdir -p rootfs
mount -t tmpfs none rootfs
# almalinux 8.8 with gcc
ipfs cat --progress=false QmaaQLJgbumjVEipHnpuG5J3ZRFRbTNV8G8v2Vfvniwx2S |tar Jxf - -C rootfs
# spack 0.22.0
ipfs cat --progress=false Qmd799ZryHUBDjB1QBp3MEPTiRbau55mJKbQr7jtKGXMT8 |tar zxf - -C rootfs/opt/

mv rootfs/opt/spack-0.22.0 rootfs/opt/spack
CONF=rootfs/opt/spack/etc/spack/defaults/config.yaml
sed -i 's:$spack/opt/spack:/opt/nbg:' $CONF
sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}_b{hash:1};' $CONF
##sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}/{hash:3};' $CONF

echo 'nameserver 119.29.29.29' > rootfs/etc/resolv.conf

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat << 'EOF' | chroot rootfs
export PATH=/opt/spack/bin:$PATH
spack compiler add
spack install aocc@4.2.0 +license-agreed
spack compiler add `spack location -i aocc@4.2.0`
spack compiler add
yes | spack module tcl refresh
EOF

umount rootfs/proc
umount rootfs/dev

rm -rf rootfs/tmp/*
cat $0 >> rootfs/.nbg
tar cf aocc-4.2.0_spack-0.22.0_almalinux-8.8.tar -C rootfs .
xz -9 -T 0 aocc-4.2.0_spack-0.22.0_almalinux-8.8.tar
rm -rf rootfs/opt/spack/var/spack/cache/_source-cache/archive/*
tar cf aocc-4.2.0_spack-0.22.0_almalinux-8.8.nocache.tar -C rootfs .
xz -9 -T 0 aocc-4.2.0_spack-0.22.0_almalinux-8.8.nocache.tar
umount rootfs
rmdir rootfs
