mkdir -p rootfs
mount -t tmpfs none rootfs
# QmSYt9d1WBNVjfgNNhyV4sNVqiM1yzVVzdXHWy63zhbGWf centos-7.6.1810-rootfs.x86_64.tar.xz
ipfs cat --progress=false QmSYt9d1WBNVjfgNNhyV4sNVqiM1yzVVzdXHWy63zhbGWf |tar Jxf - -C rootfs
# QmYELVaJXfvm659buVhY2tuPHc7pxhwrx9sb49ceZdDv3c python3.el76.tar
ipfs cat --progress=false QmYELVaJXfvm659buVhY2tuPHc7pxhwrx9sb49ceZdDv3c |tar xf - -C rootfs/tmp/
# QmXo2JwJkCmGBDg2wjbARyyHxu37rYjqHVi7XiBWKTWSCg spack-0.22.1.tar.gz
ipfs cat --progress=false QmXo2JwJkCmGBDg2wjbARyyHxu37rYjqHVi7XiBWKTWSCg |tar zxf - -C rootfs/opt/

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat << 'EOF' | chroot rootfs
mv /opt/spack-0.22.1 /opt/spack
cd /tmp/python3.el76
yum --disablerepo=* --enablerepo=el76 -y install ./*.rpm
yum clean all
EOF

umount rootfs/proc
umount rootfs/dev

rm -rf rootfs/tmp/*
cat $0 >> rootfs/.nbg
tar cf spack-0.22.1_el76.tar -C rootfs .
xz -9 -T 0 spack-0.22.1_el76.tar
umount rootfs
rmdir rootfs
