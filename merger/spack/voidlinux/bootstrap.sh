mkdir -p rootfs
mount -t tmpfs none rootfs
# QmdBP7wfKhCBGdPHByGSoY5JimqF9cUwXk22ZdKXVyhK1v void-x86_64-musl-ROOTFS-20240314.tar.xz
ipfs cat --progress=false QmdBP7wfKhCBGdPHByGSoY5JimqF9cUwXk22ZdKXVyhK1v |tar Jxf - -C rootfs
# QmXo2JwJkCmGBDg2wjbARyyHxu37rYjqHVi7XiBWKTWSCg spack-0.22.1.tar.gz
ipfs cat --progress=false QmXo2JwJkCmGBDg2wjbARyyHxu37rYjqHVi7XiBWKTWSCg |tar zxf - -C rootfs/opt/

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

cat << 'EOF' | chroot rootfs
echo nameserver 9.9.9.9 > /etc/resolv.conf
mv /opt/spack-0.22.1 /opt/spack
xbps-install -Suy python3 patch which make curl tar bzip2 gzip xz unzip git file vim openssl
python3 -m venv /opt/venv
/opt/venv/bin/pip install clingo
. /opt/venv/bin/activate
/opt/spack/bin/spack bootstrap now
EOF

umount rootfs/proc
umount rootfs/dev

rm -rf rootfs/tmp/*
cat $0 >> rootfs/.nbg
tar cf spack-0.22.1_voidlinux.tar -C rootfs .
xz -9 -T 0 spack-0.22.1_voidlinux.tar
umount rootfs
rmdir rootfs
