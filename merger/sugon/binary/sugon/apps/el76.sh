wget -c https://mirror.nju.edu.cn/centos-vault/7.6.1810/os/x86_64/Packages/centos-release-7-6.1810.2.el7.centos.x86_64.rpm
mkdir -p rootfs/var/lib/rpm
rpm --root $PWD/rootfs -ivh *.rpm --force --nodeps

rm -f rootfs/etc/yum.repos.d/*.repo
cat <<'EOF' > rootfs/etc/yum.repos.d/el76.repo
[el76]
name=el76
baseurl=http://mirror.nju.edu.cn/centos-vault/7.6.1810/os/x86_64/
gpgcheck=0
enabled=1
EOF

yum --installroot $PWD/rootfs -y --nogpgcheck install yum rootfiles
echo nameserver 119.29.29.29 > rootfs/etc/resolv.conf
mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc
chroot rootfs bash -c 'yum -y install yum rootfiles gcc kernel-headers gcc-c++ gcc-gfortran flex bison environment-modules'
umount rootfs/dev
umount rootfs/proc
rm -rf rootfs/var/cache/yum/*
tar cf centos-7.6.1810-rootfs.x86_64.tar -C rootfs .
xz -9 -T 0 centos-7.6.1810-rootfs.x86_64.tar
