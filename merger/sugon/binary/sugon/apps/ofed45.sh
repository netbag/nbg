# QmegHYfNUx5D67APYw4XRKrnHX4m5rfsmc5yZvyqd3k6kN centos-7.6.1810-rootfs.x86_64.tar.xz
mkdir -p rootfs
ipfs cat --progress=false QmegHYfNUx5D67APYw4XRKrnHX4m5rfsmc5yZvyqd3k6kN |tar Jxf - -C rootfs

cat <<'EOF' >> rootfs/etc/yum.repos.d/el76.repo
[ofed]
name=ofed
baseurl=https://linux.mellanox.com/public/repo/mlnx_ofed/4.5-1.0.1.0/rhel7.6/x86_64/
gpgcheck=0
enabled=1
EOF

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc
chroot rootfs yum --disablerepo=* --enablerepo=el76,ofed -y install ucx less strace libibverbs libmlx5 libmlx4 librdmacm infiniband-diags perftest libibverbs-utils libibmad libibumad ucx-ib ucx-rdmacm ucx-cma
umount rootfs/proc
umount rootfs/dev

rm -rf rootfs/var/cache/yum/x86_64/*
tar cf centos-7.6.1810_MLNX-4.5-1.0.1.0-rootfs.x86_64.tar -C rootfs .
xz -9 -T 0 centos-7.6.1810_MLNX-4.5-1.0.1.0-rootfs.x86_64.tar
