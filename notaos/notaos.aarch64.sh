mkdir -p rootfs
mount -t tmpfs none rootfs
# alpine mini
ipfs cat --progress=false QmdhGh7bsgSUuY5GV2Y14jNNzAV3V4meC3xvNQ4Q9pRMLT |tar Jxf - -C rootfs
# musl cross make
ipfs cat --progress=false QmZhR4k9MpwD6Tc7nMCdi8yFPs9GwscHFrQ4PbbJ7N3J73 > rootfs/opt/cross.tar.xz

echo 'nameserver 8.8.8.8' > rootfs/etc/resolv.conf
chroot rootfs sh -c 'apk add --no-progress bash musl-dev gcc linux-headers autoconf automake make sed gawk grep zlib-static zlib-dev binutils g++ upx curl wget patch'

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc
cat << 'EOF' | chroot rootfs
cd /opt/; tar Jxf cross.tar.xz
cd /opt/musl-cross-make
echo 'TARGET = aarch64-notaos-linux-musl' > config.mak
echo 'COMMON_CONFIG += CC="gcc -static --static" CXX="g++ -static --static"' >> config.mak
make -j 128
make install
rm -rf build
rm -rf /tmp/*
EOF
umount rootfs/proc
umount rootfs/dev

mv rootfs/opt/musl-cross-make/output aarch64-notaos-linux-musl
tar cf aarch64-notaos-linux-musl.aarch64.tar aarch64-notaos-linux-musl
xz -9 -T 0 aarch64-notaos-linux-musl.aarch64.tar
rm -rf aarch64-notaos-linux-musl

tar cf notaos-musl-buildenv.aarch64.tar -C rootfs .
xz -9 -T 0 notaos-musl-buildenv.aarch64.tar
umount rootfs
