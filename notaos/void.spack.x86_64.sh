#! /bin/bash

mkdir rootfs
# QmQFtLP6WGQPWEXfSRPuPnUhBmiu3mVZjhsXQryzQHZHWz voidlinux.spack-0.22.x86_64.tar.xz
ipfs cat --progress=false QmQFtLP6WGQPWEXfSRPuPnUhBmiu3mVZjhsXQryzQHZHWz | tar Jxf - -C rootfs
rm -rf rootfs/tmp/*; mkdir -p rootfs/tmp/notaos.v1
# QmNNbUBKhYtv8bTwAgz8SwsDWAoRHvvkh6aXXHp3vu7pGD openEuler-22.03-LTS-x86_64.sysroot.tar.xz
ipfs cat --progress=false QmNNbUBKhYtv8bTwAgz8SwsDWAoRHvvkh6aXXHp3vu7pGD | tar Jxf - -C rootfs/tmp/notaos.v1
# QmUA3QRdymzzjqBmuNSzZYbJpFqBSDrZhxAvE5mjamx5kc patchelf-0.16.1-x86_64
ipfs cat --progress=false QmUA3QRdymzzjqBmuNSzZYbJpFqBSDrZhxAvE5mjamx5kc > rootfs/usr/local/bin/patchelf
chmod +x rootfs/usr/local/bin/patchelf

CONF=rootfs/opt/spack/etc/spack/defaults/config.yaml
sed -i 's:$spack/opt/spack:/tmp/notaos.v1/pkg:' $CONF
#sed -i 's;{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash};{name}/{version}_{hash:2};' $CONF
sed -i 's;{name}/{version}_{hash:2};{name}/{version}{hash:1};' $CONF

mount -o bind /dev rootfs/dev
mount -t proc none rootfs/proc

echo nameserver 9.9.9.9 > rootfs/etc/resolv.conf

cat << 'EOP' > rootfs/tmp/notaos.v1/patch.sh
#! /bin/bash
SYSROOT=$PWD

find . -type f -executable | while read F ; do 
  if head -c 4 $F | grep -q ELF && patchelf --print-interpreter $F >&/dev/null; then
    patchelf --set-interpreter $SYSROOT/lib64/ld-linux-x86-64.so.2 $F 
    patchelf --set-rpath $SYSROOT/lib64:$SYSROOT/usr/lib64 $F
  fi
done

find . -type f -name 'lib*so*' | while read F; do
  if head -c 4 $F | grep -q ELF && patchelf --print-needed $F | grep -q lib; then
    patchelf --set-rpath $SYSROOT/lib64:$SYSROOT/usr/lib64 $F
  fi
done

$SYSROOT/usr/bin/gcc -dumpspecs | \
sed "/ld-linux-x86-64.so.2/s:/lib64/ld-linux-x86-64.so.2:$SYSROOT&:g" \
> $(dirname $($SYSROOT/usr/bin/gcc -print-libgcc-file-name))/specs
 
rm -f usr/bin/ld
echo -e "#! /bin/sh\nexec $SYSROOT/usr/bin/ld.bfd -rpath '\$ORIGIN/../lib:$SYSROOT/lib64:$SYSROOT/usr/lib64' \"\$@\""\
> usr/bin/ld
chmod +x usr/bin/ld

for F in gcc g++ c++ gfortran; do 
  mv usr/bin/$F usr/bin/$F.bin
  echo -e "#! /bin/sh\nexec $SYSROOT/usr/bin/$F.bin --sysroot=$SYSROOT \"\$@\"" > usr/bin/$F
  chmod +x usr/bin/$F
done

sed -i "1c#! $(which bash)" usr/bin/ldd
sed -i "/RTLDLIST=/cRTLDLIST=\"$SYSROOT/lib64/ld-linux-x86-64.so.2\"" usr/bin/ldd
EOP

cat << 'EOF' | chroot rootfs
cd /tmp/notaos.v1/
bash patch.sh
rm -f patch.sh
sed -i '154c \        default=False,' /opt/spack/var/spack/repos/builtin/packages/expat/package.py
sed -i '90i \        flags.append("-llzma")' /opt/spack/var/spack/repos/builtin/packages/libxml2/package.py
sed -i '92i \        flags.append("-llzma")' /opt/spack/var/spack/repos/builtin/packages/gettext/package.py
sed -i '364i \        flags.append("-lnghttp2")' /opt/spack/var/spack/repos/builtin/packages/curl/package.py
sed -i '538i \        flags.append("-lnghttp2 -lcrypto -lssl")' /opt/spack/var/spack/repos/builtin/packages/cmake/package.py
sed -i -e '87a\\    def flag_handler(self, name, flags):' -e '87a\        flags.append("-lintl")' -e '87a\        return (flags, None, None)' -e '87a\\' /opt/spack/var/spack/repos/builtin/packages/clingo/package.py
sed -i -e '32a\\    def flag_handler(self, name, flags):' -e '32a\        flags.append("-lunistring")' -e '32a\        return (flags, None, None)' -e '32a\\' /opt/spack/var/spack/repos/builtin/packages/libidn2/package.py
sed -i '58s/True/False/' /opt/spack/var/spack/repos/builtin/packages/openssh/package.py
rm -rf /root/.spack
. /opt/python/bin/activate
export PATH=/tmp/notaos.v1/usr/bin:/opt/spack/bin:$PATH
spack compiler add
spack install clingo patch file git environment-modules bash sed gawk grep cmake autoconf automake 
EOF

umount rootfs/dev
umount rootfs/proc

cat $0 > rootfs/.tbr
